<?php 
require_once '../_connect.php';

$timestamp = date("Y-m-d_H:i:s");

$from_date = escapeString($conn,$_POST['from_date']);
$record_by = escapeString($conn,$_POST['record_by']);
$to_date = escapeString($conn,$_POST['to_date']);
$branch = escapeString($conn,$_POST['branch']);

$diff_days = (round((strtotime($to_date) - strtotime($from_date)) / (60 * 60 * 24))) + 1;

// if(isset($_POST['btn_view']) AND $diff_days > 60)
// {
	// echo "<script>
		// alert('Max days diff value is 60 for this report !');
		// window.close();
	// </script>";
	// exit();
// }

if($branch=='ALL')
{
	$branch_var="";
}
else
{
	$branch_var="AND f2.branch='$branch'";
}

if($record_by=='LR')
{
	$date_var="f2.date";
}
else
{
	$date_var="f2.create_date";
}

if($_POST['vehicle_type']=='MARKET')
{
	$vehicle_type="and f2.frno LIKE '___F%'";
}
else if($_POST['vehicle_type']=='OWN')
{
	$vehicle_type="and f2.frno LIKE '___OLR%'";
}
else
{
	$vehicle_type="";
}

// $email = escapeString($conn,$_POST['email']);
// $email_id = escapeString($conn,$_POST['email_id']);

// ini_set('memory_limit', '-1');

$delete_ext_records = Qry($conn,"DELETE FROM fm_all");

if(!$delete_ext_records){
	echo getMySQLError($conn);
	exit();
}

$qry_copy = Qry($conn,"INSERT into fm_all(fm_no,company,lr_date,branch,lrno,tno,t_type,consignor,consignee,item,wheeler,from_stn,to_stn,lr_dest,
crossing,act_wt,charge_weight,lr_charge_wt,freight,loading,dsl_inc,gps,adv_claim,other,tds,total_freight,adv_to,adv_date,adv_party,
adv_party_pan,total_adv,adv_cash,adv_cheque,adv_diesel,adv_rtgs,balance_amount,unloading_detention,detention,gps_deposit_return,gps_rent,
gps_device,bal_tds,balance_other,claim,late_pod,total_balance,bal_to,bal_date,balance_party,balance_party_pan,bal_cash,bal_cheque,bal_diesel,
bal_rtgs,broker,broker_pan,owner,owner_pan,driver,driver_lic_no,pod_date,pod_branch,pod_copy) SELECT f2.frno,f2.company,f2.date,
f2.branch,CONCAT(\"'\",GROUP_CONCAT(f2.lrno SEPARATOR ',')),f2.truck_no,l.t_type,f2.consignor,f2.consignee,l.item,t.wheeler,f2.fstation,f2.tstation,
l.tstation,f2.crossing,ROUND(SUM(f2.wt12),2),ROUND(SUM(f2.weight),2),ROUND(SUM(l.weight),2),f.actualf,f.newtds,f.dsl_inc,f.gps,f.adv_claim,f.newother,
f.tds,f.totalf,f.ptob,f.adv_date,f.pto_adv_name,f.adv_pan,f.totaladv,f.cashadv,f.chqadv,f.disadv,f.rtgsneftamt,f.baladv,f.unloadd,f.detention,
f.gps_deposit_return,f.gps_rent,f.gps_device_charge,f.bal_tds,f.otherfr,f.claim,f.late_pod,f.totalbal,f.paidto,f.bal_date,f.pto_bal_name,f.bal_pan,
f.paycash,f.paycheq,f.paydsl,f.newrtgsamt,b.name,b.pan,t.name,t.pan,d.name,d.pan,(SELECT pod_date FROM rcv_pod where frno=f.frno ORDER BY id 
DESC LIMIT 1),(SELECT branch FROM rcv_pod where frno=f.frno ORDER by id DESC LIMIT 1),(SELECT GROUP_CONCAT(pod_copy SEPARATOR ',') 
FROM rcv_pod where frno=f.frno GROUP BY frno)
FROM freight_form_lr as f2 
LEFT OUTER JOIN freight_form as f ON f.frno = f2.frno
LEFT OUTER JOIN mk_truck as t ON t.id = f.oid 
LEFT OUTER JOIN mk_broker as b ON b.id = f.bid 
LEFT OUTER JOIN mk_driver as d ON d.id = f.did 
LEFT OUTER JOIN lr_sample as l ON l.lrno = f2.lrno 
WHERE $date_var BETWEEN '$from_date' AND '$to_date' $branch_var $vehicle_type 
GROUP by f2.frno ORDER BY f2.id ASC");

if(!$qry_copy){
	echo getMySQLError($conn);
	exit();
}

$update_rate  =  Qry($conn,"UPDATE fm_all SET hire_rate=freight/charge_weight");
if(!$update_rate){
	echo getMySQLError($conn);
	exit();
}

if(isset($_POST['btn_download']))
{
	ini_set('memory_limit',-1);

	$output='';

	$qry = Qry($conn,"SELECT * FROM fm_all");

	if(!$qry){
		echo getMySQLError($conn);
		exit();
	}

	if(numRows($qry)==0)
	{
		echo "<script>
			alert('No record found !');
			window.close();
		</script>";
		exit();
	}

	/*	
		include($_SERVER['DOCUMENT_ROOT']."/mail_lib/Classes/PHPExcel.php");
		$objPHPExcel = new PHPExcel();
		
		ini_set('memory_limit',-1);
		
		$qry  =  Qry($conn,"SELECT * FROM fm_all");
		
		if(!$qry){
			errorLog(getgetMySQLError($conn),$conn,$page_name,__LINE__);
			echo "Error !";
			exit();
		}

	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vou_No');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Company');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'LR_Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Branch');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'LR_No');
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Truck_No');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Wheeler');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'From_Loc');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'To_Loc');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'LR_Dest');
	$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'is_Crossing');
	$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Act.Wt');
	$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Chrg.Wt');
	$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'LR_Chrg.Wt');
	$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Hire_Rate');
	$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Freight(K*M)');
	$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Loading(+)');
	$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Dsl Inc(+)');
	$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'GPS(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Adv Claim(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Others(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'TDS(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Total_Freight');
	$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Adv.To');
	$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Adv.Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Adv.Party');
	$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Adv.Party_Pan');
	$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Total_Adv');
	$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Adv_Cash');
	$objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Adv_Cheque');
	$objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Adv_Diesel');
	$objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'Adv_Rtgs');
	$objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'Balance');
	$objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Unloading(+)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Detention(+)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'GPS_Rent(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'GPS_Device_Charge(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Bal_TDS(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'Bal_Other(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Bal_Claim(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Late_POD(-)');
	$objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'Total_Bal');
	$objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Bal.To');
	$objPHPExcel->getActiveSheet()->SetCellValue('AR1', 'Bal.Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('AS1', 'Bal.Party');
	$objPHPExcel->getActiveSheet()->SetCellValue('AT1', 'Bal.Party_Pan');
	$objPHPExcel->getActiveSheet()->SetCellValue('AU1', 'Bal_Cash');
	$objPHPExcel->getActiveSheet()->SetCellValue('AV1', 'Bal_Cheque');
	$objPHPExcel->getActiveSheet()->SetCellValue('AW1', 'Bal_Diesel');
	$objPHPExcel->getActiveSheet()->SetCellValue('AX1', 'Bal_Rtgs');
	$objPHPExcel->getActiveSheet()->SetCellValue('AY1', 'Broker_Name');
	$objPHPExcel->getActiveSheet()->SetCellValue('AZ1', 'Owner_Name');
	$objPHPExcel->getActiveSheet()->SetCellValue('BA1', 'Pod_Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('BB1', 'Pod_Rcvd_By');
	$objPHPExcel->getActiveSheet()->SetCellValue('BC1', 'Truck_type');
	$objPHPExcel->getActiveSheet()->SetCellValue('BD1', 'Consignor');
	$objPHPExcel->getActiveSheet()->SetCellValue('BE1', 'Item/Material');

	$objPHPExcel->getActiveSheet()->getStyle("A1:BE1")->getFont()->setBold(true);

	$rowCount	=	2;

	if(numRows($qry)==0)
	{
		echo "<br><br><center><h3 style='font-family:Verdana;'>No result found.</h3></center>";
		exit();
	}

	while($row = fetchArray($qry))
	{
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($row['fm_no'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($row['company'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($row['lr_date'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($row['branch'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($row['lrno'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($row['tno'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($row['wheeler'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, mb_strtoupper($row['from_stn'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, mb_strtoupper($row['to_stn'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, mb_strtoupper($row['lr_dest'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, mb_strtoupper($row['crossing'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, mb_strtoupper($row['act_wt'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, mb_strtoupper($row['charge_weight'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, mb_strtoupper($row['lr_charge_wt'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, mb_strtoupper($row['hire_rate'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, mb_strtoupper($row['freight'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, mb_strtoupper($row['loading'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, mb_strtoupper($row['dsl_inc'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, mb_strtoupper($row['gps'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, mb_strtoupper($row['adv_claim'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, mb_strtoupper($row['other'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, mb_strtoupper($row['tds'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, mb_strtoupper($row['total_freight'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, mb_strtoupper($row['adv_to'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, mb_strtoupper($row['adv_date'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, mb_strtoupper($row['adv_party'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, mb_strtoupper($row['adv_party_pan'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, mb_strtoupper($row['total_adv'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, mb_strtoupper($row['adv_cash'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AD'.$rowCount, mb_strtoupper($row['adv_cheque'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AE'.$rowCount, mb_strtoupper($row['adv_diesel'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AF'.$rowCount, mb_strtoupper($row['adv_rtgs'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AG'.$rowCount, mb_strtoupper($row['balance_amount'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AH'.$rowCount, mb_strtoupper($row['unloading_detention'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AI'.$rowCount, mb_strtoupper($row['detention'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AJ'.$rowCount, mb_strtoupper($row['gps_rent'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AK'.$rowCount, mb_strtoupper($row['gps_device'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AL'.$rowCount, mb_strtoupper($row['bal_tds'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AM'.$rowCount, mb_strtoupper($row['balance_other'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AN'.$rowCount, mb_strtoupper($row['claim'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AO'.$rowCount, mb_strtoupper($row['late_pod'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AP'.$rowCount, mb_strtoupper($row['total_balance'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AQ'.$rowCount, mb_strtoupper($row['bal_to'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AR'.$rowCount, mb_strtoupper($row['bal_date'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AS'.$rowCount, mb_strtoupper($row['balance_party'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AT'.$rowCount, mb_strtoupper($row['balance_party_pan'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AU'.$rowCount, mb_strtoupper($row['bal_cash'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AV'.$rowCount, mb_strtoupper($row['bal_cheque'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AW'.$rowCount, mb_strtoupper($row['bal_diesel'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AX'.$rowCount, mb_strtoupper($row['bal_rtgs'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AY'.$rowCount, mb_strtoupper($row['broker'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('AZ'.$rowCount, mb_strtoupper($row['owner'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('BA'.$rowCount, mb_strtoupper($row['pod_date'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('BB'.$rowCount, mb_strtoupper($row['pod_branch'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('BC'.$rowCount, mb_strtoupper($row['t_type'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('BD'.$rowCount, mb_strtoupper($row['consignor'],'UTF-8'));
		$objPHPExcel->getActiveSheet()->SetCellValue('BE'.$rowCount, mb_strtoupper($row['item'],'UTF-8'));

		$rowCount++;
	}

	// START - Auto size columns for each worksheet
	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

		$objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

		$sheet = $objPHPExcel->getActiveSheet();
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(true);

		foreach ($cellIterator as $cell) {
			$sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
		}
	}
	// END - Auto size columns for each worksheet

	$objWriter	=	new PHPExcel_Writer_Excel2007($objPHPExcel);

		$filename = 'Freight_Memo_'.$branch.'_to_'.$from_date.'-'.$to_date.'.xlsx';
		 
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename);
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		 // $objWriter->save('./'.$filename);	
	*/

	 $output .= '
		<table border="1">  
			<tr>  
				<th>Vou_No</th>  
				<th>Company</th>  
				<th>LR_Date</th>  
				<th>Branch</th>  
				<th>LR_No</th>  
				<th>Truck_No</th>  
				<th>Truck_Type</th>  
				<th>Consignor</th>  
				<th>Consignee</th>  
				<th>Item</th>  
				<th>Wheeler</th>  
				<th>From_Loc</th>  
				<th>To_Loc</th>  
				<th>LR_Dest</th>  
				<th>is_Crossing</th>  
				<th>Act.Wt</th>  
				<th>Chrg.Wt</th>  
				<th>LR_Chrg.Wt</th>  
				<th>Hire_Rate</th>  
				<th>Freight</th>  
				<th>Loading(+)</th>  
				<th>Dsl Inc(+)</th>  
				<th>GPS(-)</th>  
				<th>Adv Claim(-)</th>  
				<th>Others(-)</th>  
				<th>TDS(-)</th>  
				<th>Total_Freight</th>  
				<th>Adv.To</th>  
				<th>Adv.Date</th>  
				<th>Adv.Party</th>  
				<th>Adv.Party_Pan</th>  
				<th>Total_Adv</th>  
				<th>Adv_Cash</th>  
				<th>Adv_Cheque</th>  
				<th>Adv_Diesel</th>  
				<th>Adv_Rtgs</th>  
				<th>Balance</th>  
				<th>Unloading(+)</th>  
				<th>Detention(+)</th>  
				<th>GPS_Deposit_Return(+)</th>  
				<th>GPS_Rent(-)</th>  
				<th>GPS_Device_Charge(-)</th>  
				<th>Bal_TDS(-)</th>  
				<th>Bal_Other(-)</th>  
				<th>Bal_Claim(-)</th>  
				<th>Late_POD(-)</th>  
				<th>Total_Bal</th>  
				<th>Bal.To</th>  
				<th>Bal.Date</th>  
				<th>Bal.Party</th>  
				<th>Bal.Party_Pan</th>  
				<th>Bal_Cash</th>  
				<th>Bal_Cheque</th>  
				<th>Bal_Diesel</th>  
				<th>Bal_Rtgs</th>  
				<th>Broker_Name</th>  
				<th>Broker_PAN</th>  
				<th>Owner_Name</th>  
				<th>Owner_PAN</th>  
				<th>Driver_Name</th>  
				<th>Driver_Lic_No.</th>  
				<th>Pod_Date</th>  
				<th>Pod_Rcvd_By</th>  
				<th>POD_Copy</th>  
	</tr>';

	while($row = fetchArray($qry))
	{
		
		$pod_files1 = array(); 
		$copy_no = 0;
		foreach(explode(",",$row['pod_copy']) as $pod_copies)
		  {
			$copy_no++;
				  
				  if (strpos($pod_copies, 'pdf') !== false) {
				  $file = 'PDF';
				  } else {
				  $file = 'IMAGE';
				  }

				if(strpos($row["fm_no"], 'OLR') !== false)
				{
					$pod_files1[] = "<center><a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
				}
				else
				{
					$pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a> </center>";
				} 
			} 
		   
		  $podfile = implode("",$pod_files1);
		
		$output .= '
		<tr>  
			<td>'.$row["fm_no"].'</td>  
			<td>'.$row["company"].'</td>  
			<td>'.$row["lr_date"].'</td>  
			<td>'.$row["branch"].'</td>  
			<td>'.$row["lrno"].'</td>  
			<td>'.$row["tno"].'</td>  
			<td>'.$row["t_type"].'</td>  
			<td>'.$row["consignor"].'</td>  
			<td>'.$row["consignee"].'</td>  
			<td>'.$row["item"].'</td>  
			<td>'.$row["wheeler"].'</td>  
			<td>'.$row["from_stn"].'</td>  
			<td>'.$row["to_stn"].'</td>   
			<td>'.$row["lr_dest"].'</td>   
			<td>'.$row["crossing"].'</td>   
			<td>'.$row["act_wt"].'</td>  
			<td>'.$row["charge_weight"].'</td>  
			<td>'.$row["lr_charge_wt"].'</td>  
			<td>'.$row["hire_rate"].'</td>  
			<td>'.$row["freight"].'</td>  
			<td>'.$row["loading"].'</td>  
			<td>'.$row["dsl_inc"].'</td>  
			<td>'.$row["gps"].'</td>  
			<td>'.$row["adv_claim"].'</td>  
			<td>'.$row["other"].'</td>  
			<td>'.$row["tds"].'</td>  
			<td>'.$row["total_freight"].'</td>  
			<td>'.$row["adv_to"].'</td>  
			<td>'.$row["adv_date"].'</td>  
			<td>'.$row["adv_party"].'</td>  
			<td>'.$row["adv_party_pan"].'</td>  
			<td>'.$row["total_adv"].'</td>  
			<td>'.$row["adv_cash"].'</td>  
			<td>'.$row["adv_cheque"].'</td>  
			<td>'.$row["adv_diesel"].'</td>  
			<td>'.$row["adv_rtgs"].'</td>  
			<td>'.$row["balance_amount"].'</td>  
			<td>'.$row["unloading_detention"].'</td>  
			<td>'.$row["detention"].'</td>  
			<td>'.$row["gps_deposit_return"].'</td>  
			<td>'.$row["gps_rent"].'</td>  
			<td>'.$row["gps_device"].'</td>  
			<td>'.$row["bal_tds"].'</td>  
			<td>'.$row["balance_other"].'</td>  
			<td>'.$row["claim"].'</td>  
			<td>'.$row["late_pod"].'</td>  
			<td>'.$row["total_balance"].'</td>  
			<td>'.$row["bal_to"].'</td>  
			<td>'.$row["bal_date"].'</td>  
			<td>'.$row["balance_party"].'</td>  
			<td>'.$row["balance_party_pan"].'</td>  
			<td>'.$row["bal_cash"].'</td>  
			<td>'.$row["bal_cheque"].'</td>  
			<td>'.$row["bal_diesel"].'</td>  
			<td>'.$row["bal_rtgs"].'</td>  
			<td>'.$row["broker"].'</td>  
			<td>'.$row["broker_pan"].'</td>  
			<td>'.$row["owner"].'</td>  
			<td>'.$row["owner_pan"].'</td>  
			<td>'.$row["driver"].'</td>  
			<td>'.$row["driver_lic_no"].'</td>  
			<td>'.$row["pod_date"].'</td>  
			<td>'.$row["pod_branch"].'</td>  
			<td>'.$podfile.'</td>  
		</tr>
	   ';
	}

	 $output .= '</table>';
	 
	 $filename = 'FM_'.$branch.'_Branch_'.$from_date.'_to_'.$to_date.'.xls';
	 
	  header('Content-Type: application/xls');
	  header('Content-Disposition: attachment; filename='.$filename.'');
	  echo $output;
	  
	closeConnection($conn);	 
}
else
{
?>
<head>
<title>Freight Memo</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/extensions/Scroller/js/dataTables.scroller.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
</style>


<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<button type="button" onclick="window.close();" class="btn btn-sm btn-default pull-left"><span class="fa fa-window-close-o"></span> Close</button>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Freight - Memo</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
		<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>  
					<th>Vou_No</th>  
					<th>Company</th>  
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Branch</th>  
					<th>LR_No <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Truck_No <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Wheeler</th>  
					<th>From_Loc <span class="glyphicon glyphicon-filter"></span></th>  
					<th>To_Loc <span class="glyphicon glyphicon-filter"></span></th>  
					<th>LR_dest</th>  
					<th>is_Crossing</th>  
					<th>Act.Wt</th>  
					<th>Chrg.Wt</th>  
					<th>LR_Chrg.Wt</th>  
					<th>Freight</th>  
					<th>Total_Freight</th>  
					<th>Adv.To</th>  
					<th>Total_Adv</th>  
					<th>Balance</th>  
					<th>Total_Bal</th>  
					<th>Bal.To</th>  
					<th>Broker_Name <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Owner_Name <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Pod_Date <span class="glyphicon glyphicon-filter"></span></th>  
					<th>Pod_Rcvd_By <span class="glyphicon glyphicon-filter"></span></th>  
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>

	</div>
</div>
</body>
</html>

<script type="text/javascript">
function FetchLRs(){
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	], 
        "serverSide": true,
         "ajax": {
            "url": "_fetch_fm_server.php",
            "type": "POST"
        },
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchLRs();
</script>

<?php
}
?>