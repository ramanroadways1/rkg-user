<?php
require_once("_connect.php");

?>
 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>Ewb_number</th>
                        <th>Branch</th>
                        <th>Username</th> 
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT a.id,a.lrno,a.ewb_no,a.branch,a.narration,a.timestamp,
	e.name as username 
	FROM _allow_lr_ewb_expired AS a 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = a.branch_user
	WHERE a.approval_timestamp IS NULL");
	
	if(numRows($get_roles)==0)
	{ 
		echo "<tr>
			<td colspan='9'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			// $lr_date = date("d-m-y",strtotime($row['lr_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$row[ewb_no]</td>
				<td>$row[branch]</td>
				<td>$row[username]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
				  
<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>