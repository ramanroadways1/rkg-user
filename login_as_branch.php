<?php
include("_header_datatable.php");
?>

<form action="" method="POST">

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Login As Branch : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">

				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Select Branch <font color="red"><sup>* </sup></font></label>
							<select name="branch" id="branch" class="form-control" required>
								<option value="">--select branch--</option>
								<?php
								
								$get_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT in('HEAD','DUMMY') AND z!='1'");
								
								while($row_b = fetchArray($get_branches))
								{
									echo "<option value='$row_b[username]'>$row_b[username]</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" name="login_button"><i class="fa fa-sign-in" aria-hidden="true"></i> &nbsp; Login</button>
						</div>
					
					</div>
				</div>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
</form>

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<?php
if(isset($_POST['login_button']))
{
	$branch = mysqli_real_escape_string($conn,$_POST['branch']);
	
	$_SESSION['user'] = $branch;
	$_SESSION['user_code'] = "395";
	$_SESSION['role_login_type'] = "2";
	
	echo "<script>
		window.location.href='https://rrpl.online/b5aY6EZzK52NA8F';
	</script>";
}
?>