<?php 
require_once '../_connect.php';
// include('../mail_lib/Classes/PHPExcel.php');

// $objPHPExcel = new PHPExcel();

$timestamp = date("Y-m-d_H:i:s");

$from_date = escapeString($conn,$_POST['from_date']);
$record_by = escapeString($conn,$_POST['record_by']);
$to_date = escapeString($conn,$_POST['to_date']);
$branch = escapeString($conn,$_POST['branch']);
$only_shivani = escapeString($conn,$_POST['only_shivani']);

if($branch=='ALL')
{
	$branch_var="";
}
else
{
	$branch_var="AND l.branch='$branch'";
}

if($record_by=='LR')
{
	$date_var="l.date";
}
else
{
	$date_var="l.create_date";
}

ini_set('memory_limit', '-1');

if($only_shivani=='YES')
{
	if($branch=='ALL')
	{
		$qry = Qry($conn,"SELECT l.company,l.branch,l.lrno,l.lr_type,l.date,l.truck_no,l.fstation,l.tstation,l.dest_zone,l.consignor,
		l.consignee,l.po_no,l.bank,l.freight_type,l.sold_to_pay as sold_party,l.lr_name,l.wt12,l.gross_wt,l.weight,l.bill_rate,l.bill_amt,
		l.t_type,l.do_no,l.shipno,l.invno,l.item,l.articles,l.goods_desc,l.goods_value,l.con1_gst,l.con2_gst,l.hsn_code,l.round_trip_lrno 
		FROM lr_sample AS l
		WHERE $date_var BETWEEN '$from_date' AND '$to_date' AND l.truck_no in(SELECT tno FROM mk_truck WHERE pan IN('AAKCS4923M','AAMCS2305R',
		'ADAFS0507H')) ORDER BY l.id ASC");
	}
	else
	{
		$qry = Qry($conn,"SELECT l.company,l.branch,l.lrno,l.lr_type,l.date,l.truck_no,l.fstation,l.tstation,l.dest_zone,l.consignor,
		l.consignee,l.po_no,l.bank,l.freight_type,l.sold_to_pay as sold_party,l.lr_name,l.wt12,l.gross_wt,l.weight,l.bill_rate,l.bill_amt,
		l.t_type,l.do_no,l.shipno,l.invno,l.item,l.articles,l.goods_desc,l.goods_value,l.con1_gst,l.con2_gst,l.hsn_code,l.round_trip_lrno 
		FROM lr_sample AS l
		WHERE $date_var BETWEEN '$from_date' AND '$to_date' AND l.truck_no in(SELECT tno FROM mk_truck WHERE pan IN('AAKCS4923M','AAMCS2305R',
		'ADAFS0507H')) AND l.branch='$branch' ORDER BY l.id ASC");
	}
}
else
{
	$qry = Qry($conn,"SELECT l.company,l.branch,l.lrno,l.lr_type,l.date,l.truck_no,l.fstation,l.tstation,l.dest_zone,l.consignor,
	l.consignee,l.po_no,l.bank,l.freight_type,l.sold_to_pay as sold_party,l.lr_name,l.wt12,l.gross_wt,l.weight,l.bill_rate,l.bill_amt,
	l.t_type,l.do_no,l.shipno,l.invno,l.item,l.articles,l.goods_desc,l.goods_value,l.con1_gst,l.con2_gst,l.hsn_code,l.round_trip_lrno 
	FROM lr_sample AS l
	WHERE $date_var BETWEEN '$from_date' AND '$to_date' $branch_var ORDER BY l.id ASC");
}

if(!$qry){
	echo getMySQLError($conn);
	exit();
}

if(isset($_POST['btn_download']))
{

ini_set('memory_limit',-1);
	
$output = '';
	
if(numRows($qry)==0)
{
	echo "<script>
			alert('No record found !');
			window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
        <tr>  
		<th>Company</th>
		<th>Branch</th>
		<th>LR_No</th>
		<th>Round_Trip LR</th>
		<th>Vehicle_Type</th>
		<th>LR_Date</th>
		<th>Truck No</th>
		<th>From_loc</th>
		<th>To_loc</th>
		<th>Dest.Zone</th>
		<th>Consignor</th>
		<th>Consignor_GST</th>
		<th>Consignee</th>
		<th>Consignee_GST</th>
		<th>PO_No</th>
		<th>Bank</th>
		<th>Freight_Type</th>
		<th>Sold_To_Party</th>
		<th>LR_Name</th>
		<th>Act.Wt</th>
		<th>Gross_Wt</th>
		<th>Chrg.Wt</th>
		<th>Bill.Rate</th>
		<th>Bill.Amt</th>
		<th>Truck_Type</th>
		<th>Do_No</th>
		<th>Shipment_No</th>
		<th>Inv.No</th>
		<th>Item</th>
		<th>Articles</th>
		<th>Goods_Desc.</th>
		<th>Goods_Value</th>
		<th>HSN_Code</th>
	</tr>';
		
while($row = fetchArray($qry))
{
	
 $output .= '<tr> 
			<td>'.$row["company"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["round_trip_lrno"].'</td>
			<td>'.$row["lr_type"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["truck_no"].'</td>
			<td>'.$row["fstation"].'</td>
			<td>'.$row["tstation"].'</td>
			<td>'.$row["dest_zone"].'</td>
			<td>'.$row["consignor"].'</td>
			<td>'.$row["con1_gst"].'</td>
			<td>'.$row["consignee"].'</td>
			<td>'.$row["con2_gst"].'</td>
			<td>'."'".$row["po_no"].'</td>
			<td>'.$row["bank"].'</td>
			<td>'.$row["freight_type"].'</td>
			<td>'.$row["sold_party"].'</td>
			<td>'.$row["lr_name"].'</td>
			<td>'.$row["wt12"].'</td>
			<td>'.$row["gross_wt"].'</td>
			<td>'.$row["weight"].'</td>
			<td>'.$row["bill_rate"].'</td>
			<td>'.$row["bill_amt"].'</td>
			<td>'.$row["t_type"].'</td>
			<td>'."'".$row["do_no"].'</td>
			<td>'."'".$row["shipno"].'</td>
			<td>'."'".$row["invno"].'</td>
			<td>'.$row["item"].'</td>
			<td>'.$row["articles"].'</td>
			<td>'.$row["goods_desc"].'</td>
			<td>'.$row["goods_value"].'</td>
			<td>'.$row["hsn_code"].'</td>
	</tr>';
}

 $output .= '</table>';
 
 $filename = 'LR_ENTRY_'.$branch.'_Branch_'.$from_date.'_to_'.$to_date.'.xls';
 
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename='.$filename.'');
  echo $output;

closeConnection($conn);	 
}
