<?php
require_once("_connect.php");
// $type = escapeString($conn,(($_POST['type'])));
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>History : Allow Cash</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="../admin_lte/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="../admin_lte/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/skins/_all-skins.min.css">
		
		<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
		<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>
	</head>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div>
	
<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family: 'Verdana', sans-serif !important;
	font-size:11px !important;
}

table>thead>tr>th{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>	
<br />
<div class="container-fluid">
    <div class="row">
		<div class="form-group col-md-4">
			<button type="button" onclick="window.close()" class="btn btn-danger btn-sm">Close Window</button>
		</div>
		<div class="form-group col-md-4"> 
			<center><h1 style="font-size:16px;">Allowed Cash in FM Summary :</h1>
			<font color="red">Last 100 records</font>
			</center>
		</div>
		
		<div class="form-group col-md-4">
			<a target="_blank" href="./download_history_allow_cash.php"><button type="button" class="btn pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?> btn-sm"><span class="fa fa-download"></span> &nbsp; Download</button></a>
		</div>
	</div>
       
	<div class="row">
        <div class="form-group col-md-12 table-responsive">
			<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>Vou_No</th>
                        <th>Adv/Bal</th>
                        <th>Amount</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$qry = Qry($conn,"SELECT e.id,e.lrno,e.vou_no,e.adv_bal,e.lr_date,e.amount,e.narration,e.username,e.admin_timestamp,e.branch,e.timestamp,u.name 
	FROM allow_cash AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.admin_timestamp IS NOT NULL ORDER BY e.id DESC LIMIT 100");
	
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			if($row['adv_bal']=="1"){
				$adv_bal="Advance";
			}else{
				$adv_bal="Balance";
			}
			
			$admin_timestamp = date("d-m-y h:i A",strtotime($row['admin_timestamp']));
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date</td>
				<td>$row[vou_no]</td>
				<td>$adv_bal</td>
				<td>$row[amount]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td>$row[username]</td>
				<td>$admin_timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
           
    <!-- jQuery 2.1.4 -->
    <script src="../admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../admin_lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../admin_lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../admin_lte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../admin_lte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../admin_lte/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>

<script type="text/javascript">
  $(window).load(function() {
    $("#loadicon").fadeOut("slow");;
  });
</script>

<div id="func_result"></div>  