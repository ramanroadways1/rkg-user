<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$type = escapeString($conn,(trim($_POST['type'])));
$id = escapeString($conn,(trim($_POST['id'])));
$vou_no = escapeString($conn,(trim($_POST['vou_no'])));
$lr_date = escapeString($conn,(trim($_POST['lr_date'])));
$lrno = escapeString($conn,(trim($_POST['lrno'])));

$date1 = substr($vou_no,4,2);
$date2 = substr($vou_no,6,2);
$date3 = substr($vou_no,8,4);

$vou_date = $date3."-".$date2."-".$date1;

if($type=='')
{
	AlertErrorTopRight("Data not found !");
	exit();
}

if($id=='')
{
	AlertErrorTopRight("Request not found !");
	exit();
}

$get_balance_max_val = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='BALANCE_LOCK' AND is_active='1'");

if(!$get_balance_max_val){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_balance_max_val)==0) 
{
	$max_balance_days = 60;
}
else
{
	if($branch=='PEN'){
		$max_balance_days = 60;
	}else{
		$row_bal_lock_val = fetchArray($get_balance_max_val);
		$max_balance_days = $row_bal_lock_val['func_value'];
	}
}

StartCommit($conn);
$flag = true;

if($type=='approve')
{
	$approve = Qry($conn,"UPDATE rcv_pod_free SET username='$user1',is_allowed='1',allow_timestamp='$timestamp' WHERE id='$id'");

	if(!$approve){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$datediff = strtotime(date("Y-m-d")) - strtotime($vou_date);
	$diff_value=round($datediff / (60 * 60 * 24));	
						
	if($diff_value>$max_balance_days)
	{
		$chk_balance_val = Qry($conn,"SELECT id FROM extend_bal_pending_validity WHERE frno='$vou_no'");

		if(!$chk_balance_val){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($chk_balance_val)==0)
		{
			$allow_late_balance = Qry($conn,"INSERT INTO extend_bal_pending_validity(lrno,frno,lr_date,narration,admin_approval,admin_approval_timestamp,
			timestamp) VALUES ('$lrno','$vou_no','$lr_date','Allowed with LatePod','$user1','$timestamp','$timestamp')");

			if(!$allow_late_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}

	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			$('#btn_reject_$id').attr('disabled',true);
			$('#btn_allow_$id').attr('disabled',true);
			$('#btn_reject_$id').attr('onclick','');
			$('#btn_allow_$id').attr('onclick','');
			$('#btn_allow_$id').html('Approved');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertErrorTopRight("Error while processing request !");
		exit();
	}	
}
else
{
	$delete_req = Qry($conn,"DELETE FROM rcv_pod_free WHERE id='$id'");

	if(!$delete_req){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			$('#btn_reject_$id').attr('disabled',true);
			$('#btn_allow_$id').attr('disabled',true);
			$('#btn_reject_$id').attr('onclick','');
			$('#btn_allow_$id').attr('onclick','');
			$('#btn_reject_$id').html('Rejected');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertErrorTopRight("Error while processing request !");
		exit();
	}	
}	
?>