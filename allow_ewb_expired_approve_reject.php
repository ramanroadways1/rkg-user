<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$type = escapeString($conn,(trim($_POST['type'])));
$id = escapeString($conn,(trim($_POST['id'])));

if($type=='')
{
	AlertErrorTopRight("Data not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if($id=='')
{
	AlertErrorTopRight("Request not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($type=='approve')
{ 
	$approve = Qry($conn,"UPDATE _allow_lr_ewb_expired SET username='$user1',approval_timestamp='$timestamp' WHERE id='$id'");

	if(!$approve){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			$('#btn_reject_$id').attr('disabled',true);
			$('#btn_allow_$id').attr('disabled',true);
			$('#btn_reject_$id').attr('onclick','');
			$('#btn_allow_$id').attr('onclick','');
			$('#btn_allow_$id').html('Approved');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertErrorTopRight("Error while processing request !");
		exit();
	}	
}
else
{
	$delete_req = Qry($conn,"DELETE FROM _allow_lr_ewb_expired WHERE id='$id'");

	if(!$delete_req){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			$('#btn_reject_$id').attr('disabled',true);
			$('#btn_allow_$id').attr('disabled',true);
			$('#btn_reject_$id').attr('onclick','');
			$('#btn_allow_$id').attr('onclick','');
			$('#btn_reject_$id').html('Rejected');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertErrorTopRight("Error while processing request !");
		exit();
	}	
}	
?>