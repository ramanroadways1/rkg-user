<?php
include("_header.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Excel_FM') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Download : Freight Memo : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
			
			<div class="col-md-12">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="row">	
					<div class="form-group col-md-12"><div class="bg-gray col-md-12"><label style="padding-top:5px">=> Date wise : </label></div></div> 
				</div>
					
				<div class="row">
				
				<form action="download_fm_by_date.php" method="POST" target="_blank">
					
					<div class="form-group col-md-2">
						<label>Date Type <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="record_by" class="form-control" required>
							<option style="font-size:12px !important" value="">--Date Type--</option>
							<option style="font-size:12px !important" value="LR">LR Date</option>
							<option style="font-size:12px !important" value="FM">Voucher Date</option>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>Vehicle Type <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="vehicle_type" id="vehicle_type" class="form-control" required>
							<option style="font-size:12px !important" value="">--Vehicle Type--</option>
							<option style="font-size:12px !important" value="MARKET">MARKET Vehicle</option>
							<option style="font-size:12px !important" value="OWN">OWN Vehicle</option>
							<option style="font-size:12px !important" value="BOTH">MARKET + OWN Vehicle</option>
						</select>
					</div>
					
					<div class="form-group col-md-3">
						<label>From Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>	
								
					<div class="form-group col-md-3">
						<label>To Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div class="form-group col-md-2">
						<label>Branch <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" id="branch" name="branch" class="form-control" required>
							<option style="font-size:12px !important" value="ALL">ALL Branches</option>
							<?php
							$qry = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
							
							if(numRows($qry)>0)
							{
								while($row = fetchArray($qry))
								{
									echo "<option style='font-size:12px !important' value='$row[username]'>$row[username]</option>";
								}
							}
							?>
						</select>
					</div>
					
					<div class="form-group col-md-4">
						<button type="submit" name="btn_view" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; View</button>
						<button type="submit" name="btn_download" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
					</div>
				
				</form>				
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="row">	
					<div class="form-group col-md-12"><div class="bg-gray col-md-12"><label style="padding-top:5px">=> Location wise : </label></div></div> 
				</div>
					
				<div class="row">

<script>
$(function() {
		$("#from_location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_location').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_location').val("");   
			$('#from_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_location').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_location').val("");   
			$('#to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>
				
				<form action="download_fm_by_location.php" method="POST" target="_blank">
					
					<div class="form-group col-md-2">
						<label>From Location <font color="red"><sup>*</sup></font></label>
						<input type="text" id="from_location" style="font-size:12px !important" name="from_location" class="form-control" required>
					</div>
					
					<div class="form-group col-md-2">
						<label>To Location <font color="red"><sup>*</sup></font></label>
						<input type="text" id="to_location" style="font-size:12px !important" name="to_location" class="form-control" required>
					</div>
					
					<div class="form-group col-md-2">
						<label>Vehicle Type <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="vehicle_type" id="vehicle_type" class="form-control" required>
							<option style="font-size:12px !important" value="">--Vehicle Type--</option>
							<option style="font-size:12px !important" value="MARKET">MARKET Vehicle</option>
							<option style="font-size:12px !important" value="OWN">OWN Vehicle</option>
							<option style="font-size:12px !important" value="BOTH">MARKET + OWN Vehicle</option>
						</select>
					</div>
					
					<input type="hidden" name="from_id" id="from_id">
					<input type="hidden" name="to_id" id="to_id">
					
					<div class="form-group col-md-2">
						<label>From Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>	
								
					<div class="form-group col-md-2">
						<label>To Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div class="form-group col-md-2">
						<label>Branch <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" id="branch" name="branch" class="form-control" required>
							<option style="font-size:12px !important" value="ALL">ALL Branches</option>
							<?php
							$qry = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
							
							if(numRows($qry)>0)
							{
								while($row = fetchArray($qry))
								{
									echo "<option style='font-size:12px !important' value='$row[username]'>$row[username]</option>";
								}
							}
							?>
						</select>
					</div>
					
					<div class="form-group col-md-4">
						<button type="submit" name="btn_view" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; View</button>
						<button type="submit" name="btn_download" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
					</div>
				
				</form>				
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="row">	
					<div class="form-group col-md-12"><div class="bg-gray col-md-12"><label style="padding-top:5px">=> PAN wise : </label></div></div> 
				</div>
					
				<div class="row">
				
				<form action="download_fm_by_pan.php" method="POST" target="_blank">
					
					<div class="form-group col-md-2">
						<label>PAN as <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="pan_as" class="form-control" required>
							<option style="font-size:12px !important" value="ADV">Advance Party</option>
							<option style="font-size:12px !important" value="BAL">Balance Party</option>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>From Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>	
								
					<div class="form-group col-md-2">
						<label>To Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div class="form-group col-md-3">
						<label>Enter Pan <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="pan_no" type="text" class="form-control" maxlength="10" required />
					</div>	
					
					<div class="form-group col-md-3">
						<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
						<button type="submit" name="btn_view" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; View</button>
						<button type="submit" name="btn_download" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
					</div>
				
				</form>				
				</div>
				
				
			</div>
				
				<div class="col-md-12">&nbsp;</div>
			
				
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>
	
<div id="func_result"></div> 
 
<script>
function Load(type,date_type,branch,date)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "load_freight_memo.php",
		data: 'date=' + date + '&type=' + type + '&date_type=' + date_type + '&branch=' + branch,
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
		},
		error: function() {}
		});
}
</script>
