<?php
require_once("_connect.php");

$id = escapeString($conn,($_POST['id']));
$value = escapeString($conn,($_POST['value1']));
$type = escapeString($conn,($_POST['type']));

if($type!='view' AND $type!='delete' AND $type!='insert' AND $type!='update')
{
	echo "<script>alert('Error !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($value=="0")
{
	$value="1";
}
else
{
	$value="0";
}

$col_name="u_$type";

$update_role = Qry($conn,"UPDATE _access_control SET `$col_name`='$value' WHERE id='$id'");

if(!$update_role){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if($type=='view')
{
	echo "<script>$('#view_checkbox_$id').val('$value');</script>";
}
else if($type=='delete')
{
	echo "<script>$('#delete_checkbox_$id').val('$value');</script>";
}
else if($type=='insert')
{
	echo "<script>$('#insert_checkbox_$id').val('$value');</script>";
}
else if($type=='update')
{
	echo "<script>$('#update_checkbox_$id').val('$value');</script>";
}

echo "<script>$('#loadicon').fadeOut('slow');</script>";
exit();
?>