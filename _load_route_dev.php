<?php
require_once("_connect.php");

$radius = escapeString($conn,$_POST['radius']);
$frequency = escapeString($conn,$_POST['frequency']);

if(empty($radius))
{
	AlertErrorTopRight("Enter radius first !");
	exit();
}

if(empty($frequency))
{
	AlertErrorTopRight("Enter deviation frequency first !");
	exit();
}

include '../diary/route_dev/PolyUtils.php';
include '../diary/route_dev/Polyline.php';

// const MAX_TEST_POINTS = $frequency;

function getPrevState($conn, $vehicleNumber, $tripId) {
    $query = "select id, current_route_dev_state, prev_state from dairy.route_deviation_state where tno = '".$vehicleNumber."' and tripid = '".$tripId."'";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $outputArr = array();
        $outputArr['id'] = $row['id'];
        $outputArr['current_route_dev_state'] = $row['current_route_dev_state'];
        $outputArr['prev_state'] = $row['prev_state'];
        return $outputArr;
    }
    else {
        return null;
    }
}

#To get the vehicle number and the tripid of current ongoing trips
function getTripVehicles($conn) {
    $query = "select id, tno, polyline from dairy.trip where status='0' ORDER BY id DESC limit 10";
    $result = Qry($conn, $query);
    if (numRows($result) > 0) {
        $outputArr = array();

        while($row = fetchArray($result)) {
            $vehicleArr = array();

            $vehicleArr['trip_id'] = $row['id'];
            $vehicleArr['vehicleNumber'] = $row['tno'];
            $vehicleArr['polyline'] = $row['polyline'];

            $outputArr[] = $vehicleArr;
        }

        return $outputArr;
    }
    else {
        return null;
    }
}

function storePrevState ($conn, $id, $vehicleNumber, $tripId, $storePrevState, $currRouteDeviationState) {
    $now = date("Y-m-d H:i:s");

    if($id == -1) {
        $query = "insert into dairy.route_deviation_state(tno, tripid, current_route_dev_state, prev_state, created, modified)
                    values('".$vehicleNumber."', '".$tripId."', '".$currRouteDeviationState."', '".json_encode($storePrevState)."', '".$now."', '".$now."')";

        Qry($conn, $query);
    }
    else {
        $query = "update dairy.route_deviation_state set current_route_dev_state = '".$currRouteDeviationState."', prev_state = '".json_encode($storePrevState)."',
                    modified = '".$now."' where id = '".$id."'";
        Qry($conn, $query);
    }
}

function insertAlert($conn, $vehicleNumber, $tripId, $lat, $lng) {
    $now = date("Y-m-d H:i:s");
    $query = "insert into dairy.route_dev_alert(tno, tripid, deviation_lat, deviation_lng, created)
                    values('".$vehicleNumber."', '".$tripId."', '".$lat."', '".$lng."', '".$now."')";

    Qry($conn, $query);
}

function fetchCurrentLocation($vehicleNumber) {
    //http://server3.locanix.net/webservicescript/10/GetVehicleStatusByName?name=GJ18AX7902&user=ramanapiuser&pass=ramanapiuser
    $url = "http://server3.locanix.net/webservicescript/10/GetVehicleStatusByName";
    $dataArray = array("name" => $vehicleNumber, "user" => "ramanapiuser", "pass" => "ramanapiuser");
    $ch = curl_init();
    $data = http_build_query($dataArray);
    $getUrl = $url."?".$data;
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $getUrl);
    curl_setopt($ch, CURLOPT_TIMEOUT, 80);
    
    $response = curl_exec($ch);
    curl_close($ch);
 
    // if(curl_error($ch)){
        // echo 'Request Error:' . curl_error($ch);
        // return NULL;
    // }
    // else
    // {
        //echo $response;
        return $response;
    // }
}

function testRouteDeviation($prevStatesArr) {
    $allTrue = true;

    foreach ($prevStatesArr as $routeDeviationState) {
        if($routeDeviationState == false) {
            $allTrue = false;
            break;
        }
    }

    return $allTrue;
}

$vehiclesArr = getTripVehicles($conn);

foreach($vehiclesArr as $vehicle) {
    $tripId = $vehicle['trip_id'];
    $vehicleNumber = $vehicle['vehicleNumber'];
    $polyline = $vehicle['polyline'];

    $locationDataJSONStr = fetchCurrentLocation($vehicleNumber);
    if($locationDataJSONStr == NULL) {
        // print("NULL location data for ".$vehicleNumber. "\n");
        continue;
    }

    $locationDataArr = json_decode($locationDataJSONStr, true);
    $lat = $locationDataArr['latitude'];
    $lng = $locationDataArr['longitude'];
    $ign = $locationDataArr['ignition'];

    // if($ign == "Off") {
        // continue;
    // }

    $routePoints = Polyline::decode($polyline);
    $routePoints = Polyline::pair($routePoints);

    $res = isLocationOnEdgeOrPath($lat, $lng, $routePoints, $radius);

    $prevStateObj = getPrevState($conn, $vehicleNumber, $tripId);
    if($prevStateObj == null) {
        $prevStateObj = array();

        $prevState = array();
        $prevState[] = $res;
        storePrevState($conn, -1, $vehicleNumber, $tripId, $prevState, 0);
    }
    else {
        $stateId = $prevStateObj['id'];
        $prevState = json_decode($prevStateObj['prev_state']);
        $prevRouteDeviationState = $prevStateObj['current_route_dev_state'];

        #To keep only last MAX_TEST_POINTS
        if (count($prevState) >= $frequency) {
            array_shift($prevState);
        }

        $prevState[] = $res;

        if (count($prevState) == $frequency) {
            $isRouteDeviating = testRouteDeviation($prevState);
            // print "For vehicle = " . $vehicleNumber . " isRouteDeviation => " . $isRouteDeviating . "\n";

            if($prevRouteDeviationState == 0 && $isRouteDeviating == 1) {
                insertAlert($conn, $vehicleNumber, $tripId, $lat, $lng);
            }

            storePrevState($conn, $stateId, $vehicleNumber, $tripId, $prevState, $isRouteDeviating ? 1 : 0);
        } else {
            storePrevState($conn, $stateId, $vehicleNumber, $tripId, $prevState, 0);
        }
    }
}
?>

 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Trip_Date</th>
                        <th>Deviation_Lat</th>
                        <th>Deviation_Lng</th>
                        <th>From Addr</th>
                        <th>To Addr</th>
                        <th>Deviation_GMap</th>
                    </tr>
                    </thead>
                    <tbody>
<?php
$get_data = Qry($conn,"SELECT d.tno,d.deviation_lat,d.deviation_lng,t.from_station,t.to_station,t.from_poi,t.to_poi,t.start_poi_date 
FROM dairy.route_dev_alert AS d 
LEFT OUTER JOIN dairy.trip AS t ON t.id = d.tripid");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
	
if(numRows($get_data)==0)
{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
}
	else
	{
		$i=1;
		while($row = fetchArray($get_data))
		{
			$start_poi_date = date("d-m-y",strtotime($row['start_poi_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[from_station]</td>
				<td>$row[to_station]</td>
				<td>$start_poi_date</td>
				<td>$row[deviation_lat]</td>
				<td>$row[deviation_lng]</td>
				<td>$row[from_poi]</td>
				<td>$row[to_poi]</td>
				<td><a href='https://www.google.com/maps/place/$row[deviation_lat],$row[deviation_lng]' target='_blank'><button class='btn btn-xs btn-warning'>View GMap</button></a></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>

      $(function () {
        $("#example1").DataTable();
      });
	  
	    $("#loadicon").fadeOut('slow');
</script>