<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT e.id,e.lrno,e.lr_date,e.lr_branch,e.branch,e.narration,e.timestamp,e.username,e.approve_timestamp,u.name 
	FROM  allow_lr_exceed_validity AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_pending='1'");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                       <th>LR_number</th>
                        <th>LR_date</th>
                        <th>LR_branch</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
						<th>Approved_At</th>
                       
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
	  $approve_timestamp = date("d-m-y h:i A",strtotime($row['approve_timestamp']));
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			
	 
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$lr_date.'</td>
			<td>'.$row["lr_branch"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$timestamp.'</td>
			<td>'.$row["username"].'</td>
			<td>'.$approve_timestamp.'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=LR_Exceed_Validity.xls');
  echo $output;
?>