<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,(trim($_POST['lrno'])));
$vou_no1 = escapeString($conn,(trim($_POST['vou_no'])));
$vou_no = explode("_",$vou_no1)[0];
$lr_date = explode("_",$vou_no1)[1];
$truck_no = explode("_",$vou_no1)[2];
$amount = escapeString($conn,(trim($_POST['amount'])));
$cash_type = escapeString($conn,(trim($_POST['cash_type'])));
$narration = escapeString($conn,(trim($_POST['narration'])));

if($lrno=='' || $vou_no1=='' || $amount=='' || $cash_type=='' || $narration=='')
{
	AlertErrorTopRight("Fill out all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM allow_cash WHERE frno='$vou_no' AND adv_bal='$cash_type'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate record found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO allow_cash(lrno,vou_no,lr_date,amount,adv_bal,narration,username,admin_timestamp,timestamp) 
VALUES ('$lrno','$vou_no','$lr_date','$amount','$cash_type','$narration','$user1','$timestamp','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	// echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>