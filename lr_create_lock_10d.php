<?php
include("_header_datatable.php");

$lr_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='LR_Create_lock_10D') AND u_view='1'");
			  
if(numRows($lr_3)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Allow LR entry (10D Voucher not created) : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='LR_Create_lock_10D') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>					
				<div class="col-md-12">
				<div class="row">
						
						<div class="form-group col-md-3">
							<label>Branch</label>
							<select class="form-control" id="branch">
								<option value="">--select branch--</option>
							<?php
							$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");
							
							if(numRows($get_branch)>0)
							{
								while($row_branch = fetchArray($get_branch))
								{
									echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
								}
							}
							?>							
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Remind On</label>
							<input id="remind_on" name="date" type="date" class="form-control" min="<?php echo date('Y-m-d', strtotime("+1 day")); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
<?php
}
?>				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>Remind_On</th>
                        <th>Narration</th>
                        <th>Username</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT * FROM lr_lock_extend ORDER BY id DESC");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='6'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$remind_on = date("d-m-y",strtotime($row['remind_on']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$remind_on</td>
				<td>$row[narration]</td>
				<td>$row[username]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var branch = $('#branch').val();
	var remind_on = $('#remind_on').val();
	var narration = $('#narration').val();
	
	if(branch=='' || remind_on=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		// $('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_lr_create_lock_10d.php",
				data: 'branch=' + branch + '&remind_on=' + remind_on + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
