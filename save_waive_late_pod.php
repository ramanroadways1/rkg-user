<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,(trim($_POST['lrno'])));
$vou_no1 = escapeString($conn,(trim($_POST['vou_no'])));
$vou_no = explode("_",$vou_no1)[0];
$lr_date = explode("_",$vou_no1)[1];
$truck_no = explode("_",$vou_no1)[2];
$late_pod = escapeString($conn,(trim($_POST['late_pod'])));
$late_pod_amount = escapeString($conn,(trim($_POST['late_pod_amount'])));
$unloading = escapeString($conn,(trim($_POST['unloading'])));
$detention = escapeString($conn,(trim($_POST['detention'])));
$narration = escapeString($conn,(trim($_POST['narration'])));

$date1 = substr($row['vou_no'],4,2);
$date2 = substr($row['vou_no'],6,2);
$date3 = substr($row['vou_no'],8,4);

$vou_date = $date3."-".$date2."-".$date1;
			
if($lrno=='' || $vou_no1=='' || $late_pod=='' || $late_pod_amount=='' || $unloading=='' || $detention=='' || $narration=='')
{
	AlertErrorTopRight("Fill out all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM rcv_pod_free WHERE frno='$vou_no'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate record found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$get_balance_max_val = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='BALANCE_LOCK' AND is_active='1'");

if(!$get_balance_max_val){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_balance_max_val)==0) 
{
	$max_balance_days = 60;
}
else
{
	if($branch=='PEN'){
		$max_balance_days = 60;
	}else{
		$row_bal_lock_val = fetchArray($get_balance_max_val);
		$max_balance_days = $row_bal_lock_val['func_value'];
	}
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO rcv_pod_free(lrno,frno,lr_date,charges,deduct_late_pod,add_unloading,add_detention,narration,username,
is_allowed,allow_timestamp,timestamp) VALUES ('$lrno','$vou_no','$lr_date','$late_pod_amount','$late_pod','$unloading','$detention',
'$narration','$user1','1','$timestamp','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$datediff = strtotime(date("Y-m-d")) - strtotime($vou_date);
$diff_value=round($datediff / (60 * 60 * 24));	
					
if($diff_value>$max_balance_days)
{
	$allow_late_balance = Qry($conn,"INSERT INTO extend_bal_pending_validity(lrno,frno,lr_date,narration,admin_approval,admin_approval_timestamp,
	timestamp) VALUES ('$lrno','$vou_no','$lr_date','Allowed with LatePod','$user1','$timestamp','$timestamp')");

	if(!$allow_late_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	// echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>