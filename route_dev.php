<?php
include("_header_datatable.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Crossing_Report') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Route Deviation : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">

				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Radios <font color="red"><sup>* (in Meter)</sup></font></label>
							<input id="radius" name="radius" type="number" class="form-control" required />
						</div>
						
						<div class="form-group col-md-3">
							<label>Deviation Frequency <font color="red"><sup>*</sup></font></label>
							<input id="freq" name="freq" type="number" class="form-control" required />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Deviation</button>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="FlushData()" class="btn btn-sm btn-danger <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-ban" aria-hidden="true"></i> &nbsp; Flush Data</button>
						</div>
							
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function SearchFunc()
{
	var radius = $('#radius').val();
	var frequency = $('#freq').val();
	
	if(radius=='' && frequency=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter radius and frequency first !</font>',});
	}
	else
	{
		 $("#loadicon").show();
		jQuery.ajax({
			url: "_load_route_dev.php",
			data: 'radius=' + radius + '&frequency=' + frequency,
			type: "POST",
			success: function(data) {
				$("#load_table_div").html(data);
				 $('#example1').DataTable({ 
					 "destroy": true, //use for reinitialize datatable
				  });
			},
				error: function() {}
		});
	}
}

function FlushData()
{
	if(confirm("Are you sure ?"))
	{
		jQuery.ajax({
			url: "route_dev_flush_data.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
				error: function() {}
		});
	}
}
</script>