<?php
require_once("_connect.php");

?>
 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                         <th>LR_number</th>
                         <th>LR_date</th>
                         <th>Vou_number</th>
                         <th>Narration</th>
                         <th>Branch</th>
                         <th>Username</th>
                        <th>Timestamp</th>
                        <th>#Unlock</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT f.id,f.lrno,f.lr_date,f.frno,f.set_by,f.branch,f.timestamp,e.name 
	FROM block_fm as f 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = f.branch_user 
	WHERE f.active='1' ORDER by f.id ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='9'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
		
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date</td>
				<td>$row[frno]</td>
				<td>$row[set_by]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$timestamp</td>
				<td><button type='button' id='unlock_btn_$row[id]' onclick='Unlock($row[id])' class='btn btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Unlock</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
				  
<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>