<?php
require_once("_connect.php");

$id = escapeString($conn,(trim($_POST['id'])));

$unlock = Qry($conn,"UPDATE block_fm SET active='0',reactive_timestamp='$timestamp',reactive_user='$user1' WHERE id='$id'");

if(!$unlock){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

echo "<script>
	$('#unlock_btn_$id').attr('disabled',true);
	$('#unlock_btn_$id').attr('onclick','');
	$('#unlock_btn_$id').html('Unlocked');
	$('#loadicon').fadeOut('slow');
</script>";
?>