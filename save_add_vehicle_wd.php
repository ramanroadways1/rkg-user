<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper(trim($_POST['tno'])));
$branch = escapeString($conn,(trim($_POST['branch'])));
$owner_name = escapeString($conn,strtoupper(trim($_POST['owner_name'])));
$mobile_no = escapeString($conn,(trim($_POST['mobile_no'])));
$narration = escapeString($conn,(trim($_POST['narration'])));

if($tno=='' || $branch=='' || $owner_name=='' || $mobile_no=='' || $narration=='')
{
	AlertErrorTopRight("Fill out all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[A-Za-z0-9\.]*$/', $tno))
{
	AlertErrorTopRight("Invalid Vehicle Number Format !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[A-Z a-z\.]*$/', $owner_name))
{
	AlertErrorTopRight("Invalid Vehicle Owner Name !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[0-9\.]*$/', $mobile_no))
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM mk_truck WHERE tno='$tno'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate vehicle number found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO mk_truck(tno,name,mo1,branch,branch_user,narration,timestamp,doc_pending) VALUES 
('$tno','$owner_name','$mobile_no','$branch','$user1','$narration','$timestamp','1')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	// echo "<script>window.location.href='./';</script>";
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>