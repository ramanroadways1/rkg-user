<?php 
require_once '../_connect.php';

$timestamp = date("Y-m-d_H:i:s");

$till_date = escapeString($conn,$_POST['till_date']);
$get_empty_date = escapeString($conn,$_POST['get_empty_date']);
$record_type = escapeString($conn,$_POST['record_type']);

if($record_type=='ALL')
{
	if($get_empty_date=='YES')
	{
		$qry = Qry($conn,"SELECT tno,registration_date,owner_name,permanent_address,vehicle_category,maker_description,maker_model,insurance_company,
		insurance_policy_number,insurance_upto,manufacturing_date,registered_at,cubic_capacity,branch FROM rc_api");
	}
	else
	{
		$qry = Qry($conn,"SELECT tno,registration_date,owner_name,permanent_address,vehicle_category,maker_description,maker_model,insurance_company,
		insurance_policy_number,insurance_upto,manufacturing_date,registered_at,cubic_capacity,branch FROM rc_api WHERE insurance_policy_number!=''");
	}
}
else
{
	if($get_empty_date=='YES')
	{
		$qry = Qry($conn,"SELECT tno,registration_date,owner_name,permanent_address,vehicle_category,maker_description,maker_model,insurance_company,
		insurance_policy_number,insurance_upto,manufacturing_date,registered_at,cubic_capacity,branch FROM rc_api WHERE insurance_upto<='$till_date'");
	}
	else
	{
		$qry = Qry($conn,"SELECT tno,registration_date,owner_name,permanent_address,vehicle_category,maker_description,maker_model,insurance_company,
		insurance_policy_number,insurance_upto,manufacturing_date,registered_at,cubic_capacity,branch FROM rc_api WHERE insurance_upto<='$till_date' 
		AND insurance_policy_number!=''");
	}
}

if(!$qry){
	echo getMySQLError($conn);
	exit();
}
	
$output = '';
	
if(numRows($qry)==0)
{
	echo "<script>
		alert('No record found !');
		window.close();
	</script>";
	exit();
}

ini_set('memory_limit',-1);

 $output .= '
  <table border="1">    
        <tr>  
		<th>Vehicle_No</th>
		<th>Reg_Date</th>
		<th>Owner_Name</th>
		<th>Address</th>
		<th>Vehicle_Category</th>
		<th>Maker_Name</th>
		<th>Model_Name</th>
		<th>Insurance_Company</th>
		<th>Insurance_Policy_No</th>
		<th>Insurance_Exp_Date</th>
		<th>Mfg_Date</th>
		<th>Mfg_Year</th>
		<th>Registered_At</th>
		<th>Branch_Name</th>
	</tr>';
		
while($row = fetchArray($qry))
{
	$mfg_year = substr($row['manufacturing_date'], strrpos($row['manufacturing_date'], '/') + 1);	
 
 $output .= '<tr> 
		<td>'.$row["tno"].'</td>
		<td>'.$row["registration_date"].'</td>
		<td>'.$row["owner_name"].'</td>
		<td>'.$row["permanent_address"].'</td>
		<td>'.$row["vehicle_category"].'</td>
		<td>'.$row["maker_description"].'</td>
		<td>'.$row["maker_model"].'</td>
		<td>'.$row["insurance_company"].'</td>
		<td>'."'".$row["insurance_policy_number"].'</td>
		<td>'.$row["insurance_upto"].'</td>
		<td>'.$row["manufacturing_date"].'</td>
		<td>'.$mfg_year.'</td>
		<td>'.$row["registered_at"].'</td>
		<td>'.$row["branch"].'</td>
	</tr>';
}

 $output .= '</table>';
 
 $filename = 'Insurance_about_to_expire_till_'.$till_date.'.xls';
 
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename='.$filename.'');
  echo $output;

closeConnection($conn);	 

?>