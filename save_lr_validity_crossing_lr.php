<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper(trim($_POST['lrno'])));
$branch = escapeString($conn,(trim($_POST['branch'])));
$narration = escapeString($conn,(trim($_POST['narration'])));

if($lrno=='' || $branch=='' || $narration=='')
{
	AlertErrorTopRight("Fill out all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}


$chk_record = Qry($conn,"SELECT id FROM allow_lr_exceed_validity WHERE lrno='$lrno'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate record found for given LR !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$chk_lr = Qry($conn,"SELECT date,branch as lr_branch FROM lr_sample WHERE lrno='$lrno'");

if(!$chk_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_lr)==0)
{
	AlertErrorTopRight("LR not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$row = fetchArray($chk_lr);

$lr_date = $row['date'];
$lr_branch = $row['lr_branch'];

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO allow_lr_exceed_validity(lrno,lr_date,lr_branch,branch,is_pending,narration,username,approve_timestamp,timestamp) 
VALUES ('$lrno','$lr_date','$lr_branch','$branch','1','$narration','$user1','$timestamp','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	// echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>