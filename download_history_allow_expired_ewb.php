<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT l.id,l.lrno,l.ewb_no,l.narration,l.username,l.approval_timestamp,l.branch,l.timestamp,u.name 
	FROM _allow_lr_ewb_expired AS l 
	LEFT OUTER JOIN emp_attendance AS u ON u.code = l.branch_user");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                        <th>LR_No.</th>
                        <th>EWB_No.</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Req_Type</th>
                        <th>Request_Status</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
		if($row['branch']==''){
				$req_type="<font color='red'>Admin</font>";
			}else{
				$req_type="Branch";
			}
			
			if($row['approval_timestamp']=='' || $row['approval_timestamp']==0){
				$req_status="<font color='red'>Pending</font>";
			}else{
				$req_status="<font color='green'>Approved</font>";
			}		
			
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["ewb_no"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$req_type.'</td>
			<td>'.$req_status.'</td>
			<td>'.$row["username"].'</td>
			<td>'.$row["approval_timestamp"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$row["timestamp"].'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Expired_EWB_Allowed.xls');
  echo $output;
?>