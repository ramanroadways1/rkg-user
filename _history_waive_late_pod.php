<?php
require_once("_connect.php");
// $type = escapeString($conn,(($_POST['type'])));
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>History : Waiver Late POD</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="../admin_lte/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="../admin_lte/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/skins/_all-skins.min.css">
		
		<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
		<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>
	</head>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div>
	
<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family: 'Verdana', sans-serif !important;
	font-size:11px !important;
}

table>thead>tr>th{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>	
<br />
<div class="container-fluid">
    <div class="row">
		<div class="form-group col-md-4">
			<button type="button" onclick="window.close()" class="btn btn-danger btn-sm">Close Window</button>
		</div>
		<div class="form-group col-md-4"> 
			<center><h1 style="font-size:16px;">Waiver Late POD Summary :</h1>
			<font color="red">Last 100 records</font>
			</center>
		</div>
		
		<div class="form-group col-md-4">
			<a target="_blank" href="./download_history_waive_late_pod.php"><button type="button" class="btn pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?> btn-sm"><span class="fa fa-download"></span> &nbsp; Download</button></a>
		</div>
	</div>
       
	<div class="row">
        <div class="form-group col-md-12 table-responsive">
			<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date<br>POD_date</th>
                        <th>Vou_No</th>
						<th>Branch<br>Username</th>
                        <th>LatePOD</th>
                        <th>Diff. Days</td>
						<th>LatePOD Amt</th>
                        <th>Unloading</th>
						<th>Detention</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approval Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$qry = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.charges,e.deduct_late_pod,e.add_unloading,e.add_detention,e.narration,
	e.branch,e.timestamp,e.username,e.allow_timestamp,u.name,(SELECT pod_date FROM rcv_pod WHERE frno=e.frno ORDER BY pod_date DESC LIMIT 1) as pod_date 
	FROM rcv_pod_free AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_allowed='1' ORDER BY e.id DESC LIMIT 100");
	
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='13'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$allow_timestamp = date("d-m-y h:i A",strtotime($row['allow_timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			$pod_date = date("d-m-y",strtotime($row['pod_date']));
			
			$date1 = substr($row['vou_no'],4,2);
			$date2 = substr($row['vou_no'],6,2);
			$date3 = substr($row['vou_no'],8,4);

			$vou_date = $date3."-".$date2."-".$date1;
			$pod_date_db = $row['pod_date'];

			$lr_date1 = date("d-m-y",strtotime($row['lr_date']));
			$pod_date1 = date("d-m-y",strtotime($pod_date_db));

			$datediff = strtotime($pod_date_db) - strtotime($vou_date);
			$diff_value=round($datediff / (60 * 60 * 24));	
					
			if($diff_value>30)
			{
				if($diff_value>60){
					$late_pod_charges = (($diff_value-60)*100)+1500;
				}
				else{
					$late_pod_charges = ($diff_value-30)*50;
				}
			}
			else{
				$late_pod_charges = 0;
			}
															
			if($row['deduct_late_pod']=="1"){
				$deduct_late_pod="No";
				$pod_charges="0";
			}else if($row['deduct_late_pod']=="2"){
				$deduct_late_pod="Entered Value";
				$pod_charges = $row['charges'];
			}else{
				$deduct_late_pod="Sys Default";
				$pod_charges=$late_pod_charges;
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date<br>$pod_date</td>
				<td>$row[vou_no]</td>
				<td>$row[branch]<br>($row[name])</td>
				<td>$deduct_late_pod</td>
				<td>$diff_value</td>
				<td>$pod_charges</td>
				<td>$row[add_unloading]</td>
				<td>$row[add_detention]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td>$row[username]<br>$allow_timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
           
    <!-- jQuery 2.1.4 -->
    <script src="../admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../admin_lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../admin_lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../admin_lte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../admin_lte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../admin_lte/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>

<script type="text/javascript">
  $(window).load(function() {
    $("#loadicon").fadeOut("slow");;
  });
</script>

<div id="func_result"></div>  