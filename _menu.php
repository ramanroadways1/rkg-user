<?php
require_once("_connect.php");
?>

<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family: 'Verdana', sans-serif !important;
	font-size:11px !important;
}

table>thead>tr>th{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:11px !important;}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div>

<!--<div class="se-pre-con"><span class="text11">RAMAN ROADWAYS</span></div>-->
 
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

	<header class="main-header">
        <a href="./" class="logo" style="background:#FFF">
			<span class="logo-mini"><img src="../_logo/logo_raman_small.png" style="width:100%;height:50px" class="" /></span>
			<span class="logo-lg" id="logo_desktop"><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
			<span class="logo-lg" id="logo_mobile"><center><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
        </a>
    
	<nav class="navbar navbar-static-top" role="navigation">
		  <a  class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		  </a>
		  <div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			<li class="user-menu">
				<a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
				  <img src="avtar.png" class="user-image" alt="User Image">
				  <span class="hidden-xs"><?php echo $_SESSION['user_rkg']; ?></span>
					&nbsp; &nbsp; <button onclick="LogoutFunc1()" type="button" 
					class="btn btn-xs btn-danger"><i class="fa fa-power-off"></i> Logout</button></a>
				</a>
			  </li>
			</ul>
		  </div>

    </nav>
    </header>
	
<script>
function LogoutFunc1()
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		window.location.href='./logout.php';
	  }
	})
}

function CallUrl(url)
{
	if(url!='')
	{
		window.location.href=url;
	}
}
</script>	

<style>
.sidebar-menu>li>a{
	cursor:pointer;
}

.treeview>ul>li>a{
	font-family:Verdana !important;
	font-size:11px !important;
	cursor:pointer;
}

.fa-circle-o{
	font-size:10px !important
}
</style>	
     
<aside class="main-sidebar">
    <section class="sidebar">
		<div class="user-panel"></div>
	  
          <ul style="font-size:13px !important;" class="sidebar-menu" data-widget="tree">
            
			<li class="<?php if($ThisPage=="index_main.php") {echo "active";} ?>">
              <a onclick="CallUrl('./index_main.php')"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
			</li>
			
			<!--
			<li class="<?php if($ThisPage=="forms.php") {echo "active";} ?>">
              <a onclick="CallUrl('./forms.php')"><i class="fa fa-edit"></i> <span>Form Elements</span> </a>
			</li>
			-->
			
	<?php
	$check_report_cross = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('Crossing_Report')) AND u_view='1'");
	
	if(numRows($check_report_cross)>0)
	{
	?>
		<li class="<?php if($ThisPage=="report_crossing.php") {echo "active";} ?>"> 
			<a onclick="CallUrl('./report_crossing.php')"><i class="fa fa-exchange"></i> <span>Report: Crossing LRs</span> </a>
		</li>
	<?php
	}
	?>	
	
		<?php
		if($_SESSION['user_rkg']=='RISHAB' || $_SESSION['user_rkg']=='JITEN') 
		{
		?>
			<li class="<?php if($ThisPage=="login_as_branch.php") {echo "active";} ?>">
              <a onclick="CallUrl('./login_as_branch.php')"><i class="fa fa-user-circle-o"></i> <span>Login As Branch</span> </a>
			</li>
			
			<li>
              <a onclick="CallUrl('../_Verification_APIs')"><i class="fa fa-database"></i> <span>Verification APIs</span> </a>
			</li>
			
			<li class="<?php if($ThisPage=="route_dev.php") {echo "active";} ?>"> 
				<a onclick="CallUrl('./route_dev.php')"><i class="fa fa-road"></i> <span>Route Deviation</span> </a>
			</li>
			
			<li class="<?php if($ThisPage=="manage_user_rights.php") {echo "active";} ?>">
              <a onclick="CallUrl('./manage_user_rights.php')"><i class="fa fa-edit"></i> <span>Manage Users Rights</span> </a>
			</li>
			
			<li class="<?php if($ThisPage=="manage_users.php") {echo "active";} ?>">
              <a onclick="CallUrl('./manage_users.php')"><i class="fa fa-users"></i> <span>Manage Users</span> </a>
			</li>
        <?php
		}
		?>	

<?php
	$chk_rtgs_approval = Qry($conn,"SELECT id FROM extend_rtgs_approval_validity WHERE approval='0'");	
	
	$rtgs_approval_count = numRows($chk_rtgs_approval);
	
	$check1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('EWB_Exemption','Allow_expired_EWB','Off_EWB_API')) AND u_view='1'");
	
	if(numRows($check1)>0)
	{
	?>
		   <li class="treeview <?php if($ThisPage=="allow_rtgs_approval.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-newspaper-o"></i>
                <span>Rtgs Approval</span> <?php if($rtgs_approval_count>0) { echo "&nbsp; <font color='yellow'>($rtgs_approval_count)</font>"; } ?>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_rtgs1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Allow_Rtgs_Approval') AND u_view='1'");
			  
			  if(numRows($chk_rtgs1)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="allow_rtgs_approval.php") {echo "active";} ?>"><a onclick="CallUrl('./allow_rtgs_approval.php')"><i class="fa fa-circle-o"></i> Allow Approval > 30D <?php if($rtgs_approval_count>0) { echo "&nbsp; <font color='yellow'>($rtgs_approval_count)</font>"; } ?></a></li>
			  <?php
			  }
			?>	
			 </ul>
            </li>
	<?php
	}
	?>			
		
	<?php
	$count_ewb = Qry($conn,"SELECT id FROM _eway_bill_free WHERE req_pending='0'");	
	$count_ewb_expired = Qry($conn,"SELECT id FROM _allow_lr_ewb_expired WHERE approval_timestamp IS NULL");	
	
	$ewb_exempt_count = numRows($count_ewb);
	$ewb_expired_count = numRows($count_ewb_expired);
	
	$ewb_approval = $ewb_exempt_count+$ewb_expired_count;
	
	$check1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('EWB_Exemption','Allow_expired_EWB','Off_EWB_API')) AND u_view='1'");
	
	if(numRows($check1)>0)
	{
	?>
		   <li class="treeview <?php if($ThisPage=="exempt_ewb.php" || $ThisPage=="allow_expired_ewb.php" || $ThisPage=="turn_off_ewb_api.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-newspaper-o"></i>
                <span>Eway - Bill</span> <?php if($ewb_approval>0) { echo "&nbsp; <font color='yellow'>($ewb_approval)</font>"; } ?>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='EWB_Exemption') AND u_view='1'");
			  
			  if(numRows($ewb_1)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="exempt_ewb.php") {echo "active";} ?>"><a onclick="CallUrl('./exempt_ewb.php')"><i class="fa fa-circle-o"></i> Exempt Eway-bill <?php if($ewb_exempt_count>0) { echo "&nbsp; <font color='yellow'>($ewb_exempt_count)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $ewb_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Allow_expired_EWB') AND u_view='1'");
			  
			  if(numRows($ewb_2)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="allow_expired_ewb.php") {echo "active";} ?>"><a onclick="CallUrl('./allow_expired_ewb.php')"><i class="fa fa-circle-o"></i> Allow Expired Ewb <?php if($ewb_expired_count>0) { echo "&nbsp; <font color='yellow'>($ewb_expired_count)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $ewb_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Off_EWB_API') AND u_view='1'");
			  
			  if(numRows($ewb_3)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="turn_off_ewb_api.php") {echo "active";} ?>"><a onclick="CallUrl('./turn_off_ewb_api.php')"><i class="fa fa-circle-o"></i> Trun Off Ewb API</a></li>
			  <?php
			  }
			  ?>			
			 </ul>
            </li>
	<?php
	}
	?>	
	
	<?php
	$count_allow_cash = Qry($conn,"SELECT id FROM allow_cash WHERE admin_timestamp IS NULL");	
	$count_allow_late_balance = Qry($conn,"SELECT id FROM extend_bal_pending_validity WHERE admin_approval_timestamp IS NULL");	
	$count_late_pod = Qry($conn,"SELECT id FROM rcv_pod_free WHERE is_allowed='0'");	
	$count_allow_adv = Qry($conn,"SELECT id FROM advance_unlock WHERE is_allowed='0'");	
	
	$count_allow_cash = numRows($count_allow_cash);
	$count_allow_late_balance = numRows($count_allow_late_balance);
	$count_late_pod = numRows($count_late_pod);
	$count_allow_adv = numRows($count_allow_adv);
	
	$fm_approval = $count_allow_cash+$count_allow_late_balance+$count_late_pod+$count_allow_adv;
	
	$check2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('Block_FM_Balance','Allow_Cash','Unlock_FM_60D','Waive_Late_POD')) AND u_view='1'");
	
	if(numRows($check2)>0)
	{
	?>
			
			 <li class="treeview <?php if($ThisPage=="block_unblock_balance.php" || $ThisPage=="allow_cash.php" || $ThisPage=="allow_late_balance.php" || $ThisPage=="waive_late_pod.php" || $ThisPage=='unlock_advance.php') {echo "active";} ?>">
              <a >
                <i class="fa fa-newspaper-o"></i>
                <span>Freight Memo</span> <?php if($fm_approval>0) { echo "&nbsp; <font color='yellow'>($fm_approval)</font>"; } ?>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $fm_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Block_FM_Balance') AND u_view='1'");
			  
			  if(numRows($fm_1)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="block_unblock_balance.php") {echo "active";} ?>"><a onclick="CallUrl('./block_unblock_balance.php')"><i class="fa fa-circle-o"></i> Block/Unblock Balance</a></li>
			  <?php
			  }
			  
			  $fm_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Allow_Cash') AND u_view='1'");
			  
			  if(numRows($fm_2)>0)
			  {
				?>
				<li class="<?php if($ThisPage=="allow_cash.php") {echo "active";} ?>"><a onclick="CallUrl('./allow_cash.php')"><i class="fa fa-circle-o"></i> Allow Cash in FM <?php if($count_allow_cash>0) { echo "&nbsp; <font color='yellow'>($count_allow_cash)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $fm_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Unlock_FM_60D') AND u_view='1'");
			  
			  if(numRows($fm_3)>0)
			  {
				?>
				<li class="<?php if($ThisPage=="allow_late_balance.php") {echo "active";} ?>"><a onclick="CallUrl('./allow_late_balance.php')"><i class="fa fa-circle-o"></i> Allow Late Balance - 60D <?php if($count_allow_late_balance>0) { echo "&nbsp; <font color='yellow'>($count_allow_late_balance)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $fm_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Waive_Late_POD') AND u_view='1'");
			  
			  if(numRows($fm_4)>0)
			  {
				?>
				<li class="<?php if($ThisPage=="waive_late_pod.php") {echo "active";} ?>"><a onclick="CallUrl('./waive_late_pod.php')"><i class="fa fa-circle-o"></i> Waive Late POD <?php if($count_late_pod>0) { echo "&nbsp; <font color='yellow'>($count_late_pod)</font>"; } ?></a></li>
			  <?php
			  }
			  
			   $fm_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Unblock_FM_Advance') AND u_view='1'");
			
			if(numRows($fm_5)>0)
			  {
				?>
				<li class="<?php if($ThisPage=="unlock_advance.php") {echo "active";} ?>"><a onclick="CallUrl('./unlock_advance.php')"><i class="fa fa-circle-o"></i> Unlock FM Advance <?php if($count_allow_adv>0) { echo "&nbsp; <font color='yellow'>($count_allow_adv)</font>"; } ?></a></li>
			  <?php
			  }
              ?>
              </ul>
            </li>
	<?php
	}
	?>	
	
	<?php
	$check3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('Extend_LR_Val','POD_Send_Rcv_Lock','LR_Create_lock_10D','Late_POD_45D')) AND u_view='1'");
	
	$count_lr_validity = Qry($conn,"SELECT id FROM allow_lr_exceed_validity WHERE is_pending='0'");	
	$count_pod_loc = Qry($conn,"SELECT id FROM user WHERE pod_lock='1'");	
	$check_45_days_pod = Qry($conn,"SELECT id FROM _pending_lr WHERE lr_45_days>0 AND (remind_on=0 OR remind_on<='".date("Y-m-d")."')");
	$check_10_days_lr_loc = Qry($conn,"SELECT * FROM lr_sample_pending WHERE date(timestamp)>'2021-01-22' AND 
	timestamp < DATE_SUB(NOW(), INTERVAL 10 DAY) AND branch NOT IN(SELECT branch FROM lr_lock_extend WHERE remind_on>'".date("Y-m-d")."') 
	GROUP by branch");
	
	$count_45_days = numRows($check_45_days_pod);
	$count_lr_validity = numRows($count_lr_validity);
	$count_pod_loc = numRows($count_pod_loc);
	$count_10_d_lock = numRows($check_10_days_lr_loc);
	
	$lr_approval = $count_lr_validity + $count_pod_loc + $count_45_days + $count_10_d_lock;
	
	if(numRows($check3)>0)
	{
	?>
			 <li class="treeview <?php if($ThisPage=="lr_validity_crossing_lr.php" || $ThisPage=="pod_send_rcv_lock.php" || $ThisPage=="lr_create_lock_10d.php" || $ThisPage=="late_pod_45_days.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-newspaper-o"></i>
                <span>LR Lock</span> <?php if($lr_approval>0) { echo "&nbsp; <font color='yellow'>($lr_approval)</font>"; } ?>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  
			  <?php
			  $lr_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Extend_LR_Val') AND u_view='1'");
			  
			  if(numRows($lr_1)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="lr_validity_crossing_lr.php") {echo "active";} ?>"><a onclick="CallUrl('./lr_validity_crossing_lr.php')"><i class="fa fa-circle-o"></i> LR Validity (CrossingLR) <?php if($count_lr_validity>0) { echo "&nbsp; <font color='yellow'>($count_lr_validity)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $lr_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='POD_Send_Rcv_Lock') AND u_view='1'");
			  
			  if(numRows($lr_2)>0)
			  {
				?>
				<li class="<?php if($ThisPage=="pod_send_rcv_lock.php") {echo "active";} ?>"><a onclick="CallUrl('./pod_send_rcv_lock.php')"><i class="fa fa-circle-o"></i> POD Send/Rcv Lock <?php if($count_pod_loc>0) { echo "&nbsp; <font color='yellow'>($count_pod_loc)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $lr_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='LR_Create_lock_10D') AND u_view='1'");
			  
			  if(numRows($lr_3)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="lr_create_lock_10d.php") {echo "active";} ?>"><a onclick="CallUrl('./lr_create_lock_10d.php')"><i class="fa fa-circle-o"></i> LR Create Lock(10D) <?php if($count_10_d_lock>0) { echo "&nbsp; <font color='yellow'>($count_10_d_lock)</font>"; } ?></a></li>
			  <?php
			  }
			  
			  $lr_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='1005' AND func_name='Late_POD_45D') AND u_view='1'");
			  
			  if(numRows($lr_4)>0)
			  {
				?>
                <li class="<?php if($ThisPage=="late_pod_45_days.php") {echo "active";} ?>"><a onclick="CallUrl('./late_pod_45_days.php')"><i class="fa fa-circle-o"></i> Late POD (45D) <?php if($count_45_days>0) { echo "&nbsp; <font color='yellow'>($count_45_days)</font>"; } ?></a></li>
			  <?php
			  }
			  ?>
			  </ul>
            </li>
	<?php
	}
	?>	
	
	<?php
	$check_report = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('Report_FM','Report_Exp_Vou','Report_Truck_Vou','Report_Rtgs','Report_Diesel')) AND u_view='1'");
	
	if(numRows($check_report)>0)
	{
	?>
	
	<li class="treeview <?php if($ThisPage=="report_fm.php" || $ThisPage=="report_exp_vou.php" || $ThisPage=="report_truck_vou.php" || $ThisPage=="report_rtgs.php" || $ThisPage=="report_diesel.php") {echo "active";} ?>">
        <a>
           <i class="fa fa-bookmark-o"></i> <span>Reports </span> <i class="fa fa-angle-left pull-right"></i>
        </a>
            <ul class="treeview-menu">
				<li class="<?php if($ThisPage=="report_fm.php") {echo "active";} ?>"><a onclick="CallUrl('./report_fm.php')"><i class="fa fa-circle-o"></i> Freight Memo</a></li>
				<li class="<?php if($ThisPage=="report_exp_vou.php") {echo "active";} ?>"><a onclick="CallUrl('./report_exp_vou.php')"><i class="fa fa-circle-o"></i> Expense Vou.</a></li>
				<li class="<?php if($ThisPage=="report_truck_vou.php") {echo "active";} ?>"><a onclick="CallUrl('./report_truck_vou.php')"><i class="fa fa-circle-o"></i> Truck Vou.</a></li>
				<!--<li class="<?php if($ThisPage=="report_diesel.php") {echo "active";} ?>"><a onclick="CallUrl('./report_diesel.php')"><i class="fa fa-circle-o"></i> Diesel</a></li>
				<li class="<?php if($ThisPage=="report_rtgs.php") {echo "active";} ?>"><a onclick="CallUrl('./report_rtgs.php')"><i class="fa fa-circle-o"></i> Rtgs</a></li>
			--></ul>
    </li>
	<?php
	}
	?>	
	
	<?php
	$check_excel = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name IN('Excel_FM','Excel_TDV','Excel_EXP','Excel_LR')) AND u_view='1'");
	
	if(numRows($check_excel)>0)
	{
	?>
	
	<li class="treeview <?php if($ThisPage=="excel_fm.php" || $ThisPage=="excel_vehicle_rc.php" || $ThisPage=="excel_ins_exp.php" || $ThisPage=="excel_lr.php") {echo "active";} ?>">
        <a>
           <i class="fa fa-file-excel-o"></i> <span>Excel Downloads </span> <i class="fa fa-angle-left pull-right"></i>
        </a>
            <ul class="treeview-menu">
				<li class="<?php if($ThisPage=="excel_fm.php") {echo "active";} ?>"><a onclick="CallUrl('./excel_fm.php')"><i class="fa fa-circle-o"></i> FM / OTF</a></li>
				<li class="<?php if($ThisPage=="excel_lr.php") {echo "active";} ?>"><a onclick="CallUrl('./excel_lr.php')"><i class="fa fa-circle-o"></i> LR Entry</a></li>
				<li class="<?php if($ThisPage=="excel_vehicle_rc.php") {echo "active";} ?>"><a onclick="CallUrl('./excel_vehicle_rc.php')"><i class="fa fa-circle-o"></i> Vehicle RC</a></li>
				<li class="<?php if($ThisPage=="excel_ins_exp.php") {echo "active";} ?>"><a onclick="CallUrl('./excel_ins_exp.php')"><i class="fa fa-circle-o"></i> Insurance Expiry</a></li>
			</ul>
    </li>
	<?php
	}
	?>	
	
	<?php
	$check4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name='Add_Truck_WD') AND u_view='1'");
	
	if(numRows($check4)>0)
	{
	?>
	<li class="<?php if($ThisPage=="add_vehicle_wd.php") {echo "active";} ?>">
		<a onclick="CallUrl('./add_vehicle_wd.php')"><i class="fa fa-automobile"></i> <span>Add Market Veh. W/D</span> </a>
	</li>
	<?php
	}
	?>
	
	<?php
	$check5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name='RTO_Portal') AND u_view='1'");
	
	if(numRows($check5)>0)
	{
	?>
	<li class="<?php if($ThisPage=="rto_portal.php") {echo "active";} ?>">
		<a onclick="CallUrl('../OWN_TRUCK_DATA')"><i class="fa fa-truck"></i> <span>RTO Portal</span> </a>
	</li>
	<?php
	}
	?>
	
	<?php
	$check6 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='1005' AND func_name='Asset_Emp_Mgt') AND u_view='1'");
	 
	if(numRows($check6)>0)
	{
	?>
	<li class="<?php if($ThisPage=="asset_and_emp_mgt.php") {echo "active";} ?>">
		<a onclick="CallUrl('../admin/_ho_manager')"><i class="fa fa-bar-chart"></i> <span>Assets & Emp. Mgt.</span> </a>
	</li>
	<?php
	}
	?>
	
	<li>
		<a onclick="LogoutFunc1();"><i class="fa fa-power-off"></i> <span>Logout</span> </a>
	</li>
			<!--
            <li class="treeview">
              <a >
                <i class="fa fa-pie-chart"></i>
                <span>Charts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="chart/chartjs.php"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                <li><a href="chart/morris.php"><i class="fa fa-circle-o"></i> Morris</a></li>
                <li><a href="chart/flot.php"><i class="fa fa-circle-o"></i> Flot</a></li>
                <li><a href="chart/inline.php"><i class="fa fa-circle-o"></i> Inline charts</a></li>
              </ul>
            </li>
         
            <li class="treeview">
              <a >
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a ><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a ><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a ><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a ><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a ><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a ><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
			-->
			
          </ul>
        </section>
    </aside>