<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.is_allowed,e.lr_date,e.narration,e.branch,e.timestamp,e.username,e.approval_timestamp,u.name 
	FROM advance_unlock AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_allowed='1' ORDER BY e.id DESC");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                       <th>LR_number</th>
                        <th>LR_date</th>
                        <th>Vou_No</th>
                        <th>Diff. Days</td>
						<th>Status</th>
						<th>Branch</th>
						<th>User</th>
						<th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                       
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
		if($row['is_allowed']=="1"){
				$status="<font color='green'>Approved</font>";
			}else{
				$status="<font color='red'>Pending</font>";
			}
			
			$approval_timestamp = date("d-m-y h:i A",strtotime($row['approval_timestamp']));
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			
			$date1 = substr($row['vou_no'],4,2);
			$date2 = substr($row['vou_no'],6,2);
			$date3 = substr($row['vou_no'],8,4);

			$vou_date = $date3."-".$date2."-".$date1;
			
			$datediff = strtotime(date("Y-m-d")) - strtotime($vou_date);
			$diff_value=round($datediff / (60 * 60 * 24));	
					
			if($diff_value>20){
				$diff_days = "<font color='red'><b>$diff_value<b></font>";
			}
			else{
				$diff_days = "$diff_value";
			}
			
			
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["lr_date"].'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$diff_days.'</td>
			<td>'.$status.'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$row["timestamp"].'</td>
			<td>'.$row["username"].'</td>
			<td>'.$row["approval_timestamp"].'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Unlock_FM_Advance.xls');
  echo $output;
?>