<?php
include("_header_datatable.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Report_FM') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Freight Memo : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-3">
						<label>Balance Info <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="type" onchange="$('#date_sel').val('')" id="type" class="form-control" required>
							<option style="font-size:12px !important" value="NO">NO</option>
							<option style="font-size:12px !important" value="YES">YES</option>
						</select>
					</div>
					
					<div class="lrno_div form-group col-md-3">
						<label>By Date <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="date_type" onchange="$('#date_sel').val('');MyVal(this.value)" id="date_type" class="form-control" required>
							<option style="font-size:12px !important" value="FM">FM Date</option>
							<option style="font-size:12px !important" value="ADV">Adv Date</option>
							<option style="font-size:12px !important" value="BAL">Bal Date</option>
						</select>
					</div>
					
					<script>
					function MyVal(elem){
						if(elem=='BAL'){
							$('#type').val('YES');
						}
					}
					</script>
						
					<div class="lrno_div form-group col-md-3">
						<label>Branch <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" id="branch" onchange="$('#date_sel').val('')" name="branch" class="form-control" required>
							<option style="font-size:12px !important" value="ALL">ALL Branches</option>
							<?php
							$qry = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
							
							if(numRows($qry)>0)
							{
								while($row = fetchArray($qry))
								{
									echo "<option style='font-size:12px !important' value='$row[username]'>$row[username]</option>";
								}
							}
							?>
						</select>
					</div>
					
					<div class="lrno_div form-group col-md-3">
						<label>Select Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" type="date" id="date_sel" onchange="Load($('#type').val(),$('#date_type').val(),$('#branch').val(),this.value)" 
						value="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" 
						class="form-control" required max="<?php echo date("Y-m-d"); ?>">
					</div>
					
					<?php
					/*
					<div class="form-group col-md-3">
						<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
						<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						<a href="_history_ewb_exempted.php" target="_blank"><button type="button" class="btn btn-sm pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
					</div>
					*/?>
					
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
			
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div> 
 
<script>
function Load(type,date_type,branch,date)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "load_freight_memo.php",
		data: 'date=' + date + '&type=' + type + '&date_type=' + date_type + '&branch=' + branch,
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
		},
		error: function() {}
		});
}
</script>

<script>
Load('NO','FM','ALL','<?php echo date("Y-m-d"); ?>');
</script>