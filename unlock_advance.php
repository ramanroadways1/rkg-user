<?php
include("_header_datatable.php");

$fm_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unblock_FM_Advance') AND u_view='1'");
			  
if(numRows($fm_4)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">FM Advance Unlock : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-2">
						<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
						<a href="_history_unlock_fm_advance.php" target="_blank"><button type="button" class="btn btn-sm btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
					</div>
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>Vou_No</th>
                        <th>Diff. Days</td>
						<th>Branch & User</th>
						<th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.narration,e.branch,e.timestamp,u.name 
	FROM advance_unlock AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_allowed='0'");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			
			$date1 = substr($row['vou_no'],4,2);
			$date2 = substr($row['vou_no'],6,2);
			$date3 = substr($row['vou_no'],8,4);

			$vou_date = $date3."-".$date2."-".$date1;
			
			$datediff = strtotime(date("Y-m-d")) - strtotime($vou_date);
			$diff_value=round($datediff / (60 * 60 * 24));	
					
			if($diff_value>20)
			{
				$diff_days = "<font color='red'><b>$diff_value<b></font>";
			}
			else
			{
				$diff_days = "$diff_value";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date</td>
				<td>$row[vou_no]</td>
				<td>$diff_days</td>
				<td>$row[branch]<br>($row[name])</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

 <?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unblock_FM_Advance') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?>  
<script>

function Approve(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_approve_reject_fm_advance.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}

</script>


<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unblock_FM_Advance') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>

<script>

function Reject(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_approve_reject_fm_advance.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>