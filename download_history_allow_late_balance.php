<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.narration,e.branch,e.admin_approval,e.admin_approval_timestamp,
	e.timestamp,u.name 
	FROM extend_bal_pending_validity AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.admin_approval_timestamp IS NOT NULL");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>POD_date</th>
						<th>Days_Diff.</th>
                        <th>Vou_No</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                       
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
	  
	  $diff_days = round((strtotime(date("Y-m-d")) - strtotime($row['lr_date'])) / (60 * 60 * 24))+1;
			
			if($diff_days>90){
				$diff_days = "<font color='red'><b>$diff_days</b></font>";	
			}
		
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["lr_date"].'</td>
			<td>'.$row["pod_date"].'</td>
			<td>'.$diff_days.'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$row["timestamp"].'</td>
			<td>'.$row["admin_approval"].'</td>
			<td>'.$row["admin_approval_timestamp"].'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Allowed_Late_Balance.xls');
  echo $output;
?>