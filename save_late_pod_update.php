<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$deduct_late_pod = escapeString($conn,(($_POST['deduct_late_pod'])));
$late_pod_amount = escapeString($conn,(($_POST['late_pod_amount'])));
$late_pod_entered_db = escapeString($conn,(($_POST['late_pod_amount_entered'])));
$late_pod_default_db = escapeString($conn,(($_POST['late_pod_amount_default'])));
$id = escapeString($conn,(($_POST['id'])));

if($deduct_late_pod=="1"){
	$deduct_late_pod_html="No";
}else if($deduct_late_pod=="2"){
	$deduct_late_pod_html="Yes : Entered Value <b>$late_pod_amount</b>";
}else{
	$deduct_late_pod_html="Yes : Sys Default <b>$late_pod_default_db</b>";
}
			
if($deduct_late_pod=='')
{
	AlertErrorTopRight("Deduction selection not found !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

if($late_pod_amount<0)
{
	AlertErrorTopRight("Please check amount !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

if($deduct_late_pod=="2" AND $late_pod_amount<=0)
{
	AlertErrorTopRight("Please check amount !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT deduct_late_pod,charges FROM rcv_pod_free WHERE id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)==0)
{
	AlertErrorTopRight("No Record Found !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

$row = fetchArray($chk_record);

if($row['deduct_late_pod']==$deduct_late_pod AND $row['charges']==$late_pod_amount)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE rcv_pod_free SET charges='$late_pod_amount',deduct_late_pod='$deduct_late_pod' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record updated successfully !");
	echo "<script>
		$('#pod_row_$id').html('$deduct_late_pod_html<br><button type=\'button\'  style=\'margin-top:4px\' id=\'btn_modify_$id\' onclick=\'ModifyLatePOD($id)\' class=\'btn btn-warning btn-xs\'>Modify</button>');
		$('#late_pod_selection_$id').val('$deduct_late_pod');
		$('#late_pod_amount_entered_db_value$id').val('$late_pod_amount');
		$('#late_pod_amount_default_db_value$id').val('$late_pod_default_db');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#update_button_modal').attr('disabled', false);</script>";
	exit();
}	
?>