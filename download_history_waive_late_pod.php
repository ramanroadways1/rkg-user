<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.charges,e.deduct_late_pod,e.add_unloading,e.add_detention,e.narration,
	e.branch,e.timestamp,e.username,e.allow_timestamp,u.name,(SELECT pod_date FROM rcv_pod WHERE frno=e.frno ORDER BY pod_date DESC LIMIT 1) as pod_date 
	FROM rcv_pod_free AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_allowed='1'");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                       <th>LR_number</th>
                        <th>LR_date</th>
						<th>POD_date</th>
                        <th>Vou_No</th>
						<th>Branch</th>
						<th>Username</th>
                        <th>LatePOD</th>
                        <th>Diff. Days</td>
						<th>LatePOD Amt</th>
                        <th>Unloading</th>
						<th>Detention</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                       
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
	  $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
	$allow_timestamp = date("d-m-y h:i A",strtotime($row['allow_timestamp']));
	$lr_date = date("d-m-y",strtotime($row['lr_date']));
	  $pod_date = date("d-m-y",strtotime($row['pod_date']));
			
			$date1 = substr($row['vou_no'],4,2);
			$date2 = substr($row['vou_no'],6,2);
			$date3 = substr($row['vou_no'],8,4);

			$vou_date = $date3."-".$date2."-".$date1;
			$pod_date_db = $row['pod_date'];

			$lr_date1 = date("d-m-y",strtotime($row['lr_date']));
			$pod_date1 = date("d-m-y",strtotime($pod_date_db));

			$datediff = strtotime($pod_date_db) - strtotime($vou_date);
			$diff_value=round($datediff / (60 * 60 * 24));	
					
			if($diff_value>30)
			{
				if($diff_value>60){
					$late_pod_charges = (($diff_value-60)*100)+1500;
				}
				else{
					$late_pod_charges = ($diff_value-30)*50;
				}
			}
			else{
				$late_pod_charges = 0;
			}
															
			if($row['deduct_late_pod']=="1"){
				$deduct_late_pod="No";
				$pod_charges="0";
			}else if($row['deduct_late_pod']=="2"){
				$deduct_late_pod="Entered Value";
				$pod_charges = $row['charges'];
			}else{
				$deduct_late_pod="Sys Default";
				$pod_charges=$late_pod_charges;
			}
		
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$lr_date.'</td>
			<td>'.$pod_date.'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$deduct_late_pod.'</td>
			<td>'.$diff_value.'</td>
			<td>'.$pod_charges.'</td>
			<td>'.$row["add_unloading"].'</td>
			<td>'.$row["add_detention"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$timestamp.'</td>
			<td>'.$row["username"].'</td>
			<td>'.$allow_timestamp.'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Waived_Late_POD.xls');
  echo $output;
?>