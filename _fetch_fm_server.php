<?php
require_once '../_connect.php'; 

$sql ="SELECT id,fm_no as frno,company,lr_date,branch,lrno,tno,wheeler,from_stn,to_stn,lr_dest,crossing,act_wt,charge_weight,lr_charge_wt,
freight,total_freight,adv_to,adv_date,total_adv,balance_amount,total_balance,bal_to,bal_date,broker,owner,pod_date,pod_branch FROM fm_all";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'frno', 'dt' => 1),
    array( 'db' => 'company', 'dt' => 2),
    array( 'db' => 'lr_date', 'dt' => 3), 
    array( 'db' => 'branch', 'dt' => 4), 
    array( 'db' => 'lrno', 'dt' => 5),  
    array( 'db' => 'tno', 'dt' => 6),  
    array( 'db' => 'wheeler', 'dt' => 7),
    array( 'db' => 'from_stn', 'dt' => 8), 
    array( 'db' => 'to_stn', 'dt' => 9), 
    array( 'db' => 'lr_dest', 'dt' => 10), 
    array( 'db' => 'crossing', 'dt' => 11), 
    array( 'db' => 'act_wt', 'dt' => 12), 
    array( 'db' => 'charge_weight', 'dt' => 13), 
    array( 'db' => 'lr_charge_wt', 'dt' => 14), 
    array( 'db' => 'freight', 'dt' => 15), 
    array( 'db' => 'total_freight', 'dt' => 16), 
    array( 'db' => 'adv_to', 'dt' => 17), 
    array( 'db' => 'total_adv', 'dt' => 18), 
    array( 'db' => 'balance_amount', 'dt' => 19), 
    array( 'db' => 'total_balance', 'dt' => 20), 
    array( 'db' => 'bal_to', 'dt' => 21), 
    array( 'db' => 'broker', 'dt' => 22), 
    array( 'db' => 'owner', 'dt' => 23), 
    array( 'db' => 'pod_date', 'dt' => 24), 
    array( 'db' => 'pod_branch', 'dt' => 25), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);