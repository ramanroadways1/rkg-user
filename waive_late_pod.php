<?php
include("_header_datatable.php");

$fm_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Waive_Late_POD') AND u_view='1'");
			  
if(numRows($fm_4)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Waive late POD & Add Unloading/Detention : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Waive_Late_POD') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>				
				<div class="col-md-12">
				<div class="row">
						
<script>			
function FetchVou(lrno)
{
	if(lrno!='')
	{
		$('#lrno').attr('readonly',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "_fetch_vou_no_by_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#vou_no").html(data);
		},
			error: function() {}
		});
	}
}
</script>						
						
						<div class="form-group col-md-3">
							<label>LR No.</label>
							<input autocomplete="off" onblur="FetchVou(this.value)" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Vou.No.</label>
							<select class="form-control" id="vou_no">
								<option value="">--select voucher--</option>
							</select>
						</div>
						
						<script>
						function LatePODFunc(elem)
						{
							$('#late_pod_amount').val('');
							
							if(elem=='2'){
								$('#late_pod_amount').attr('readonly',false);
							}
							else{
								$('#late_pod_amount').attr('readonly',true);
							}
						}
						</script>
						
						<div class="form-group col-md-3">
							<label>LatePOD</label>
							<select class="form-control" onchange="LatePODFunc(this.value)" id="late_pod">
								<option value="">--select--</option>
								<option value="0">Yes - System Default</option>
								<option value="2">Yes - Entered Value</option>
								<option value="1">No Deduction</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>LatePOD Amount</label>
							<input autocomplete="off" id="late_pod_amount" type="number" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Add Unloading</label>
							<input autocomplete="off" id="unloading" type="number" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Add Detention</label>
							<input autocomplete="off" id="detention" type="number" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
							<a href="_history_waive_late_pod.php" target="_blank"><button type="button" class="btn btn-sm pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
<?php
}
?>
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date<br>POD_date</th>
                        <th>Vou_No<br>Branch & Username</th>
                        <th>LatePOD</th>
                        <th>Diff. Days</td>
						<th>LatePOD Amt</th>
                        <th>POD_Copy</th>
                        <th>Unloading/Detention</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.charges,e.deduct_late_pod,e.add_unloading,e.add_detention,e.narration,
	e.branch,e.timestamp,u.name,(SELECT pod_date FROM rcv_pod WHERE frno=e.frno ORDER BY pod_date DESC LIMIT 1) as pod_date,
	(SELECT GROUP_CONCAT(pod_copy SEPARATOR ',') FROM rcv_pod WHERE frno=e.frno GROUP BY e.frno) as pod_copy	
	FROM rcv_pod_free AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.is_allowed='0'");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='13'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			$pod_date = date("d-m-y",strtotime($row['pod_date']));
			
			$date1 = substr($row['vou_no'],4,2);
			$date2 = substr($row['vou_no'],6,2);
			$date3 = substr($row['vou_no'],8,4);

			$vou_date = $date3."-".$date2."-".$date1;
			$pod_date_db = $row['pod_date'];

			$lr_date1 = date("d-m-y",strtotime($row['lr_date']));
			$pod_date1 = date("d-m-y",strtotime($pod_date_db));

			$datediff = strtotime($pod_date_db) - strtotime($vou_date);
			$diff_value=round($datediff / (60 * 60 * 24));	
					
			if($diff_value>30)
			{
				if($diff_value>60){
					$late_pod_charges = (($diff_value-60)*100)+1500;
				}
				else{
					$late_pod_charges = ($diff_value-30)*50;
				}
			}
			else{
				$late_pod_charges = 0;
			}
															
			if($row['deduct_late_pod']=="1"){
				$deduct_late_pod="No";
			}else if($row['deduct_late_pod']=="2"){
				$deduct_late_pod="Yes : Entered Value <b>$row[charges]</b>";
			}else{
				$deduct_late_pod="Yes : Sys Default <b>$late_pod_charges</b>";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date<br>$pod_date</td>
				<td>$row[vou_no]<br>($row[branch])<br>$row[name]</td>
				
				<input type='hidden' value='$row[charges]' id='late_pod_charge_entered_$row[id]'>
				<input type='hidden' value='$late_pod_charges' id='late_pod_charge_default_$row[id]'>
				<input type='hidden' value='$row[deduct_late_pod]' id='late_pod_selection_$row[id]'>
				<input type='hidden' value='$row[lrno]' id='lrno_$row[id]'>
				<input type='hidden' value='$row[vou_no]' id='vou_no_$row[id]'>
				<input type='hidden' value='$row[lr_date]' id='lr_date_$row[id]'>
				
				<td id='pod_row_$row[id]'>$deduct_late_pod<br><button style='margin-top:7px' type='button' id='btn_modify_$row[id]' onclick='ModifyLatePOD($row[id])' class='btn btn_approve btn-warning btn-xs'><i class='fa fa-edit' aria-hidden='true'></i> Modify</button></td>
				";
				if($diff_value>90)
				{
					echo "<td style='color:red'>$diff_value days</td>
					<td style='color:red'>$late_pod_charges/-</td>";
				}
				else
				{
					echo "<td>$diff_value days</td>
					<td>$late_pod_charges/-</td>";
				}
				
				if($row['pod_copy']=='')
				{
					echo "<td><font color='red'>NA</font>";
				}
				else
				{
					echo "<td>";
					$sn=1;
					foreach(explode(",",$row['pod_copy']) as $pod_copy)
					{
						echo "<button type='button' style='margin-top:5px !important' onclick=PODViewModal('$pod_copy','$sn','$row[vou_no]') class='btn btn-xs btn-warning'>POD : $sn</button><br>";
						$sn++;	
					}
				}
				// pod_copy	
				echo "</td>
				<td>Unloading : $row[add_unloading]<br>Detention : $row[add_detention]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var lrno = $('#lrno').val();
	var vou_no = $('#vou_no').val();
	var late_pod = $('#late_pod').val();
	var late_pod_amount = $('#late_pod_amount').val();
	var unloading = $('#unloading').val();
	var detention = $('#detention').val();
	var narration = $('#narration').val();
	
	if(lrno=='' || vou_no=='' || late_pod=='' || late_pod_amount=='' || unloading=='' || detention=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_waive_late_pod.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&late_pod=' + late_pod + '&late_pod_amount=' + late_pod_amount + '&unloading=' + unloading + '&detention=' + detention + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>

 <?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Waive_Late_POD') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?>  
  
<script>	
function ModifyLatePOD(id)
{
	var late_pod_selection = $('#late_pod_selection_'+id).val();
	var late_pod_charge_entered = Number($('#late_pod_charge_entered_'+id).val());
	var late_pod_charge_default = Number($('#late_pod_charge_default_'+id).val());
	var lrno = $('#lrno_'+id).val();
	var vou_no = $('#vou_no_'+id).val();
	
	$('#id_modal').val(id);
	$('#late_pod_amount_entered_db_value').val(late_pod_charge_entered);
	$('#late_pod_amount_default_db_value').val(late_pod_charge_default);
	
	$('#deduct_late_pod_modal').val(late_pod_selection);
	$('#vou_details').html("Vou_No: "+vou_no+", LR_Number: "+lrno);
	LatePODSelection(late_pod_selection);
	$('#ModalButton')[0].click();
}

</script>


<script>

function Approve(id)
{
	var vou_no = $('#vou_no_'+id).val();
	var lr_date = $('#lr_date_'+id).val();
	
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "waive_late_pod_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve' + '&vou_no=' + vou_no + '&lr_date=' + lr_date,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}

</script>


<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Waive_Late_POD') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>

<script>

function Reject(id)
{
	var vou_no = $('#vou_no_'+id).val();
	var lr_date = $('#lr_date_'+id).val();
	
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "waive_late_pod_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve' + '&vou_no=' + vou_no + '&lr_date=' + lr_date,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>

<script>
function PODViewModal(path,sn,vou_no)
{
	if(path=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>POD not uploaded !</font>',});
	}
	else
	{
		$('#loadicon').show();
		$("#myModal iframe").attr("src","https://rrpl.online/b5aY6EZzK52NA8F/"+path);
		$("#vou_no_html").html(vou_no);
		$("#copy_sn_html").html(sn);
		$("#PodModalBtn")[0].click();
		$('#loadicon').fadeOut('slow');
	}
}
</script>

<!-- POD VIEW MODAL CODE -->

<button type="button" id="PodModalBtn" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content" style="max-height: calc(100vh - 70px);overflow: auto;">

      <div class="bg-primary modal-header">
        <h4 class="modal-title" style="font-size:13px;color:#FFF">View POD Copy: <span style="color:" id="copy_sn_html"></span>, Vou_No: <span style="color:" id="vou_no_html"></span></h4>
      </div>
	<div class="modal-body" id="modal_body_custom">
		<iframe class="responsive-iframe" src=""></iframe>
    </div>

      <div class="modal-footer">
        <button type="button" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
       </div>
	</div>
  </div>
</div>	

<style>
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}

#modal_body_custom {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
</style>

<!-- POD VIEW MODAL CODE -->

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#ModifyChargesModal"></button>

<form id="PODUpdateForm" autocomplete="off">
<div class="modal" id="ModifyChargesModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
			<h4 style="font-size:15px;" class="modal-title">Update Late POD : <span style="color:blue" id="vou_details"></span></h4>
      </div>
	 <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<script>
			function LatePODSelection(late_pod_selection)
			{
				var amount_default = Number($('#late_pod_amount_default_db_value').val());
				var amount_enterd = Number($('#late_pod_amount_entered_db_value').val());
				
				if(late_pod_selection=="1"){
					$('#late_pod_amount_modal').val("0");
					$('#late_pod_amount_modal').attr('readonly',true);
				}
				else if(late_pod_selection=="2"){
					$('#late_pod_amount_modal').val(amount_enterd);
					$('#late_pod_amount_modal').attr('readonly',false);
				}
				else{
					$('#late_pod_amount_modal').val(amount_default);
					$('#late_pod_amount_modal').attr('readonly',true);
				}
			}
			</script>
			
			<div class="form-group col-md-12">
				<label>Deduct Late POD : <font color="red">*</font></label>
				<select name="deduct_late_pod" id="deduct_late_pod_modal" onchange="LatePODSelection(this.value)" class="form-control" required>
					<option value="">--Select Type--</option>
					<option value="0">Yes - System Default</option>
					<option value="2">Yes - Entered Value</option>
					<option value="1">No Deduction</option>
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<label>Late POD Amount <font color="red">*</font></label>
				<input type="number" min="0" id="late_pod_amount_modal" class="form-control" name="late_pod_amount" readonly required />
			</div>
			
		</div>
			<input type="hidden" id="id_modal" name="id">	
			<input type="hidden" id="late_pod_amount_entered_db_value" name="late_pod_amount_entered">	
			<input type="hidden" id="late_pod_amount_default_db_value" name="late_pod_amount_default">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="update_button_modal" class="btn btn-sm btn-primary">Update</button>
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script type="text/javascript">
$(document).ready(function (e) {
$("#PODUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
$("#update_button_modal").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_late_pod_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
