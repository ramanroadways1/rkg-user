<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,(($_POST['id'])));
$remind_on = escapeString($conn,(($_POST['remind_on'])));


StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE _pending_lr SET remind_on='$remind_on' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated successfully !");
	// echo "<script>$('#add_btn').attr('disabled',false);</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	// echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}	
?>