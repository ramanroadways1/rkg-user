<?php
include("_header_datatable.php");

$lr_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Late_POD_45D') AND u_view='1'");
			  
if(numRows($lr_4)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Late POD 45 days : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<button type="button" class="btn pull-right btn-primary btn-xs" onclick="PODRefresh()"><i class="fa fa-refresh" aria-hidden="true"></i> &nbsp; Refresh POD data</button>
				<br />
				<br />
		
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Late_POD_45D') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?> 
<script>
function Update(id)
{
	var remind_on = $('#remind_on_'+id).val();
	
	if(remind_on=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select date first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "save_late_pod_45_days.php",
				data: 'id=' + id + '&remind_on=' + remind_on,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}

</script>

<?php
}
else
{
	echo "<script>$('.btn_allow').attr('disabled',true);</script>";
}
?>

<script> 
function PODRefresh()
{
	$('#loadicon').show();
	jQuery.ajax({
	url: "https://rrpl.online/b5aY6EZzK52NA8F/clear_45_days_lrs_refresh.php",
	data: 'id=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#func_result").html(data);
	},
		error: function() {}
	});
}

function Load_pods()
{
	$('#loadicon').show();
	jQuery.ajax({
	url: "_load_late_pod45.php",
	data: 'id=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
	},
		error: function() {}
	});
}

function ViewLRs(type1,branch)
{
	$('#type').val(type1);
	$('#branch_name').val(branch);
	$('#FormViewLR')[0].submit();
}

Load_pods();
</script>

<form target="_blank" action="_view_pending_lrs.php" method="POST" id="FormViewLR">
	<input type="hidden" id="type" name="type">
	<input type="hidden" id="branch_name" name="branch">
</form>