<?php
require_once("_connect.php");

$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);
$branch = escapeString($conn,$_POST['branch']);

if($from_date=='')
{
	AlertErrorTopRight("Select from date !");
	exit();
}
else if($to_date=='')
{
	AlertErrorTopRight("Select to date !");
	exit();
}
else if($branch=='')
{
	AlertErrorTopRight("Select branch !");
	exit();
}
?>

 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vou_No</th>
                        <th>Company</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>LR_Date</th>
                        <th>Create_Date</th>
                        <th>Vehicle_No</th>
                        <th>LR_No</th>
                        <th>From</th>
                        <th>To</th>
                        <th>LR_From</th>
                        <th>LR_To</th>
                        <th>Consignor</th>
                        <th>Consignee</th>
                        <th>Cross_Station</th>
                        <th>Timestamp</th>
                    </tr>
                    </thead>
                    <tbody>
	<?php
	
if($branch=='ALL')
{
	$get_data = Qry($conn,"SELECT f.frno,f.company,f.branch,f.date,f.create_date,f.truck_no,f.lrno,f.fstation,f.tstation,l.fstation as lr_from,
	l.tstation as lr_to,f.consignor,f.consignee,f.cross_to,f.timestamp,e.name as username 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = f.branch_user 
	WHERE f.lrno in(SELECT lrno FROM freight_form_lr WHERE date BETWEEN '$from_date' AND '$to_date' AND crossing='YES') ORDER BY f.lrno,f.id ASC");
}	
else
{
	$get_data = Qry($conn,"SELECT f.frno,f.company,f.branch,f.date,f.create_date,f.truck_no,f.lrno,f.fstation,f.tstation,l.fstation as lr_from,
	l.tstation as lr_to,f.consignor,f.consignee,f.cross_to,f.timestamp,e.name as username 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = f.branch_user 
	WHERE f.lrno in(SELECT lrno FROM freight_form_lr WHERE date BETWEEN '$from_date' AND '$to_date' AND branch='$branch' AND crossing='YES') ORDER BY f.lrno,f.id ASC");
}
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_data)==0)
	{
		echo "<tr>
			<td colspan='17'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_data))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['date']));
			$create_date = date("d-m-y",strtotime($row['create_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>
					<form target='_blank' action='../_view/freight_memo.php' method='POST'>
						<input type='hidden' value='$row[frno]' name='value1'>
						<input type='hidden' value='SEARCH' name='key'>
						<a href='#' onclick='this.parentNode.submit();' style='color:maroon;font-weight:bold'>$row[frno]</a>
					</form>
				</td>
				<td>$row[company]</td>
				<td>$row[branch]</td>
				<td>$row[username]</td>
				<td>$lr_date</td>
				<td>$create_date</td>
				<td>$row[truck_no]</td>
				<td>$row[lrno]</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td>$row[lr_from]</td>
				<td>$row[lr_to]</td>
				<td>$row[consignor]</td>
				<td>$row[consignee]</td>
				<td>$row[cross_to]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>

      $(function () {
        $("#example1").DataTable();
      });
	  
	    $("#loadicon").fadeOut('slow');
</script>