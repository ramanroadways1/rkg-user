<?php
require_once("_connect.php");

$lrno = escapeString($conn,(trim($_POST['lrno'])));

$get_voucher = Qry($conn,"SELECT frno,date,truck_no FROM freight_form_lr WHERE lrno = '$lrno' AND frno like '___F%' GROUP by frno");

if(!$get_voucher){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($get_voucher)==0)
{
	echo "<option value=''>--select voucher--</option>";
	
	echo "<script>$('#lrno').attr('readonly',false);</script>";
}
else
{
	while($row = fetchArray($get_voucher))
	{
		echo "<option value='$row[frno]_$row[date]_$row[truck_no]'>$row[frno] - $row[truck_no] : $row[date]</option>";
	}
	
	echo "<script>$('#lrno').attr('readonly',true);</script>";
}

echo "<script>$('#loadicon').fadeOut('slow');</script>";
?>