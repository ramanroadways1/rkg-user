<?php
include("_header_datatable.php");

$fm_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Block_FM_Balance') AND u_view='1'");
			  
if(numRows($fm_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>


<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Block - Unblock FM Balance : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
				<div class="row">
						
<script>			
function FetchVou(lrno)
{
	if(lrno!='')
	{
		$('#lrno').attr('readonly',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "_fetch_vou_no_by_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#vou_no").html(data);
		},
			error: function() {}
		});
	}
}
</script>						
						
						<div class="form-group col-md-3">
							<label>LR number</label>
							<input autocomplete="off" onblur="FetchVou(this.value)" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Voucher number</label>
							<select class="form-control" id="vou_no">
								<option value="">--select voucher--</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var lrno = $('#lrno').val();
	var vou_no = $('#vou_no').val();
	var narration = $('#narration').val();
	
	if(lrno=='' || vou_no=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_block_unblock_balance.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
  
<script>	
function Unlock(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "unlock_fm_balance.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_block_unblock_fm.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
			 $('#example1').DataTable({ 
                 "destroy": true, //use for reinitialize datatable
              });
		},
		error: function() {}
	});
}

LoadTable();
</script>