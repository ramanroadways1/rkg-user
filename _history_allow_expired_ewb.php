<?php
require_once("_connect.php");
// $type = escapeString($conn,(($_POST['type'])));
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>History : Allow expired EWB</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="../admin_lte/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="../admin_lte/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/skins/_all-skins.min.css">
		
		<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
		<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>
	</head>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div>
	
<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family: 'Verdana', sans-serif !important;
	font-size:11px !important;
}

table>thead>tr>th{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>	
<br />
<div class="container-fluid">
    <div class="row">
		<div class="form-group col-md-4">
			<button type="button" onclick="window.close()" class="btn btn-danger btn-sm">Close Window</button>
		</div>
		<div class="form-group col-md-4">
			<center><h1 style="font-size:16px;">Allow expired EWB :</h1>
			<font color="red">Last 100 records</font>
			</center>
		</div>
		
		<div class="form-group col-md-4">
			<a target="_blank" href="./download_history_allow_expired_ewb.php"><button type="button" class="btn pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?> btn-sm"><span class="fa fa-download"></span> &nbsp; Download</button></a>
		</div>
	</div>
       
	<div class="row">
        <div class="form-group col-md-12 table-responsive">
			<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_No.</th>
                        <th>EWB_No.</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Req_Type</th>
                        <th>Request_Status</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
					  </tr>
                    </thead>
                    <tbody>
	<?php
	$qry = Qry($conn,"SELECT l.id,l.lrno,l.ewb_no,l.narration,l.username,l.approval_timestamp,l.branch,l.timestamp,u.name 
	FROM _allow_lr_ewb_expired AS l 
	LEFT OUTER JOIN emp_attendance AS u ON u.code = l.branch_user 
	ORDER BY l.id DESC LIMIT 100");
	
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='11'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y H:i A",strtotime($row['timestamp']));
			
			if($row['approval_timestamp']==0 || $row['approval_timestamp']==''){
				$approve_timestamp = "NA";
			}else{
				$approve_timestamp = date("d-m-y H:i A",strtotime($row['approval_timestamp']));
			}
			
			// if($row['lr_date']==0){
				// $lr_date = "NA";
			// }else{
				// $lr_date = $lr_date = date("d-m-y",strtotime($row['lr_date']));
			// }
			
			if($row['branch']==''){
				$req_type="<font color='red'>Admin</font>";
			}else{
				$req_type="Branch";
			}
			
			if($row['approval_timestamp']=='' || $row['approval_timestamp']==0){
				$req_status="<font color='red'>Pending</font>";
			}else{
				$req_status="<font color='green'>Approved</font>";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$row[ewb_no]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$req_type</td>
				<td>$req_status</td>
				<td>$row[username]</td>
				<td>$approve_timestamp</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
           
    <!-- jQuery 2.1.4 -->
    <script src="../admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../admin_lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../admin_lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../admin_lte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../admin_lte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../admin_lte/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>

<script type="text/javascript">
  $(window).load(function() {
    $("#loadicon").fadeOut("slow");;
  });
</script>

<div id="func_result"></div>  