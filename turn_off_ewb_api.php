<?php
include("_header_datatable.php");

$ewb_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Off_EWB_API') AND u_view='1'");
			  
if(numRows($ewb_3)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Turn off EWB API : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Off_EWB_API') AND u_insert='1'");
			  
if(numRows($ewb_insert)>0)
{
?>				
				<div class="col-md-12">
				<div class="row">
					
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				<?php
				}
				?>
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
// function AddRecordFunc()
// {
	// var lrno = $('#lrno').val();
	// var ewb_no = $('#ewb_no').val();
	// var branch = $('#branch').val();
	// var narration = $('#narration').val();
	
	// if(lrno=='' || ewb_no=='' || branch=='' || narration=='')
	// {
		// Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	// }
	// else
	// {
		// $('#add_btn').attr('disabled',true);
			// $('#loadicon').show();
			// jQuery.ajax({
				// url: "save_allow_expired_ewb.php",
				// data: 'lrno=' + lrno + '&ewb_no=' + ewb_no + '&branch=' + branch + '&narration=' + narration,
				// type: "POST",
				// success: function(data) {
				// $("#func_result").html(data);
				// },
				// error: function() {}
		// });
	// }
// }
</script>
  
<script>	
// function Approve(id)
// {
	// $('#loadicon').show();
	// jQuery.ajax({
		// url: "allow_ewb_expired_approve_reject.php",
		// data: 'id=' + id + '&type=' + 'approve',
		// type: "POST",
		// success: function(data) {
		// $("#func_result").html(data);
		// },
		// error: function() {}
	// });
// }

// function Reject(id)
// {
	// $('#loadicon').show();
	// jQuery.ajax({
		// url: "allow_ewb_expired_approve_reject.php",
		// data: 'id=' + id + '&type=' + 'reject',
		// type: "POST",
		// success: function(data) {
		// $("#func_result").html(data);
		// },
		// error: function() {}
	// });
// }
</script>	

<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_turn_off_ewb_api.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
			 $('#example1').DataTable({ 
                 "destroy": true, //use for reinitialize datatable
              });
		},
		error: function() {}
	});
}

LoadTable();
</script>