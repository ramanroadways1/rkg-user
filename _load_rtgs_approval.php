<?php
require_once("_connect.php");

?>
 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vou_No</th>
                        <th>CRN</th>
                        <th>Ac_Holder</th>
                        <th>Amount</th>
                        <th>Vou_Date</th>
                        <th>Payment_Date</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT e.id,e.crn,e.branch,e.narration,r.acname,r.fno,r.amount,r.pay_date,r.fm_date,e.timestamp,u.name 
	FROM extend_rtgs_approval_validity AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	LEFT OUTER JOIN rtgs_fm as r ON r.id = e.rtgs_id 
	WHERE e.approval='0'");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='13'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$vou_date = date("d-m-y",strtotime($row['fm_date']));
			$pay_date = date("d-m-y",strtotime($row['pay_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[fno]</td>
				<td>$row[crn]</td>
				<td>$row[acname]</td>
				<td>$row[amount]</td>
				<td>$vou_date</td>
				<td>$pay_date</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>

      $(function () {
        $("#example1").DataTable();
      });
</script>