<?php
include("_header_datatable.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Crossing_Report') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Crossing Report : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">

				<div class="col-md-12">
				<div class="row">
					
					<div class="form-group col-md-3">
						<label>From date <font color="red"><sup>*</sup></font></label>
						<input id="from_date" name="from_date" onchange="CallFromDate(this.value)" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div class="form-group col-md-3">
						<label>To date <font color="red"><sup>*</sup></font></label>
						<input id="to_date" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<script>
					function CallFromDate(fromdate)
					{
						var nextDay = new Date(fromdate);
						nextDay.setDate(nextDay.getDate() + 0);
						var next_day=nextDay.toISOString().slice(0,10);
						$("#to_date").attr("min",nextDay.toISOString().slice(0,10));
						$("#to_date").val("");
					}
					</script>
						
						<div class="form-group col-md-3">
							<label>Select branch <font color="red"><sup>* (LR Branch)</sup></font></label>
							<select class="form-control" required="required" id="branch" name="branch">
								<option value="">--select branch--</option>
								<option value="ALL">ALL BRANCH</option>
								<?php
								$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY')");
								
								if(numRows($get_branch) > 0)
								{
									while($row_branch = fetchArray($get_branch))
									{
										echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
									}
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function SearchFunc()
{
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	var branch = $('#branch').val();
	
	if(from_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select from date first !</font>',});
	}
	else if(to_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select to date first !</font>',});
	}
	else if(branch=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select branch first !</font>',});
	}
	else
	{
		 $("#loadicon").show();
		jQuery.ajax({
			url: "_load_report_crossing.php",
			data: 'from_date=' + from_date + '&to_date=' + to_date + '&branch=' + branch,
			type: "POST",
			success: function(data) {
				$("#load_table_div").html(data);
				 $('#example1').DataTable({ 
					 "destroy": true, //use for reinitialize datatable
				  });
			},
				error: function() {}
		});
	}
}
</script>

<script>
function ViewVoucher(frno)
{
	alert(frno);
}
</script>