<?php
require_once("_connect.php");

$branch = escapeString($conn,$_POST['branch']);
$date = escapeString($conn,$_POST['date']);
?>
<table id="example1" style="font-size:11px !important" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Vou_No</th>
                                            <th>Branch</th>
                                            <th>Date</th>
                                            <th>RR/RRPL</th>
                                            <th>Truck_No</th>
                                            <th>Driver<br>Name</th>
                                            <th>Payment<br>By</th>
                                            <th>Amount</th>
										</tr>
                                    </thead>
									
                                    <tbody>
                                        <?php
										if($branch=='ALL')
										{
											$qry = Qry($conn,"SELECT user,company,tdvid,newdate,truckno,dname,amt,mode 
											FROM mk_tdv WHERE newdate='$date' ORDER by user ASC,id ASC");
										}
										else
										{
											$qry = Qry($conn,"SELECT user,company,tdvid,newdate,truckno,dname,amt,mode 
											FROM mk_tdv WHERE newdate='$date' AND user='$branch' ORDER by user ASC,id ASC");
										}
										
										if(numRows($qry)>0)
										{
											$i=1;
											while($row=fetchArray($qry))
											{
												echo "<tr>
													<td>$i</td>
													<td>
														<form target='_blank' action='../_view/vouchers.php' method='POST'>
															<input type='hidden' value='$row[tdvid]' name='vou_no'>
															<input type='submit' class='btn btn-primary btn-xs' value='$row[tdvid]' />
														</form>
													</td>
													<td>$row[user]</td>
													<td>$row[newdate]</td>
													<td>$row[company]</td>
													<td>$row[truckno]</td>
													<td>$row[dname]</td>
													<td>$row[mode]</td>
													<td>$row[amt]</td>
												</tr>";
											$i++;	
											}
										}
										else
										{
											echo "<tr>
													<td>NO RESULT FOUND.</td>
													<td></td>
													<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
												</tr>";
										}
										?>
                                    </tbody>
                                </table>

<script>										
	$('#loadicon').fadeOut('slow');
</script>								

<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>							
