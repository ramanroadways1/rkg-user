<?php
include("_header_datatable.php");

$fm_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Cash') AND u_view='1'");
			  
if(numRows($fm_2)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Allow Cash in FM : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Cash') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>				
				<div class="col-md-12">
				<div class="row">
						
<script>			
function FetchVou(lrno)
{
	if(lrno!='')
	{
		$('#lrno').attr('readonly',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "_fetch_vou_no_by_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#vou_no").html(data);
		},
			error: function() {}
		});
	}
}
</script>						
						
						<div class="form-group col-md-3">
							<label>LR No.</label>
							<input autocomplete="off" onblur="FetchVou(this.value)" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Vou.No.</label>
							<select class="form-control" id="vou_no">
								<option value="">--select voucher--</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Amount</label>
							<input autocomplete="off" min="1" type="number" class="form-control" id="amount" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Type</label>
							<select class="form-control" id="cash_type">
								<option value="">--select type--</option>
								<option value="1">Advance Cash</option>
								<option value="2">Balance Cash</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<a href="_history_allow_cash.php" target="_blank"><button type="button" class="btn btn-sm pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				<?php
				}
				?>
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>Vou_No</th>
                        <th>Adv/Bal</th>
                        <th>Amount</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT e.id,e.lrno,e.vou_no,e.adv_bal,e.lr_date,e.amount,e.narration,e.branch,e.timestamp,u.name 
	FROM allow_cash AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.admin_timestamp IS NULL");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			if($row['adv_bal']=="1"){
				$adv_bal="Advance";
			}else{
				$adv_bal="Balance";
			}
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date</td>
				<td>$row[vou_no]</td>
				<td>$adv_bal</td>
				<td>$row[amount]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var lrno = $('#lrno').val();
	var vou_no = $('#vou_no').val();
	var amount = $('#amount').val();
	var cash_type = $('#cash_type').val();
	var narration = $('#narration').val();
	
	if(lrno=='' || vou_no=='' || amount=='' || cash_type=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_allow_cash.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&amount=' + amount + '&cash_type=' + cash_type + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
 

<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Cash') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?> 
<script>	
function Approve(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "allow_cash_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>
<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Cash') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>
function Reject(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "allow_cash_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>