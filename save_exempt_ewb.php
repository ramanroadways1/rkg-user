<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$exempt_by=escapeString($conn,(trim($_POST['exempt_by'])));
$lrno=escapeString($conn,strtoupper(trim($_POST['lrno'])));
$lrno_from=escapeString($conn,(trim($_POST['lrno_from'])));
$lrno_to=escapeString($conn,(trim($_POST['lrno_to'])));

if($exempt_by=='')
{
	AlertErrorTopRight("Please select exempt by option first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if($exempt_by=='LR_NO')
{
	if($lrno=='')
	{
		AlertErrorTopRight("Enter LR number first !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
	
	if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
	{
		AlertErrorTopRight("Invalid LR number !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
	
	$chk_record = Qry($conn,"SELECT id FROM _eway_bill_free WHERE lrno='$lrno'");

	if(!$chk_record){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while processing request !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}

	if(numRows($chk_record)>0)
	{
		AlertErrorTopRight("Duplicate record found !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
}
else if($exempt_by=='LR_SERIES')
{
	if($lrno_from=='' || $lrno_to=='')
	{
		AlertErrorTopRight("Please enter LR from and LR to value !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
	
	if(!preg_match('/^[0-9\.]*$/', $lrno_from))
	{
		AlertErrorTopRight("Invalid value for from LR series !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
	
	if(!preg_match('/^[0-9\.]*$/', $lrno_to))
	{
		AlertErrorTopRight("Invalid value for to LR series !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
	
	$chk_record = Qry($conn,"SELECT id FROM _eway_bill_free WHERE lrno>=$lrno_from AND lrno<=$lrno_to");

	if(!$chk_record){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while processing request !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}

	if(numRows($chk_record)>0)
	{
		AlertErrorTopRight("Duplicate record found in LR series !");
		echo "<script>$('#add_btn').attr('disabled', false);</script>";
		exit();
	}
}
else
{
	AlertErrorTopRight("Invalid option selected !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($exempt_by=='LR_NO')
{
	$insert = Qry($conn,"INSERT INTO _eway_bill_free(lrno,check_type,status,username,approve_timestamp,req_pending,timestamp) 
	VALUES ('$lrno','3','1','$user1','$timestamp','1','$timestamp')");

	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	for($i=$lrno_from;$i<=$lrno_to;$i++)
	{
		$insert = Qry($conn,"INSERT INTO _eway_bill_free(lrno,check_type,status,username,approve_timestamp,req_pending,timestamp) VALUES 
		('$i','3','1','$user1','$timestamp','1','$timestamp')");

		if(!$insert){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>