<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$ewb_no = escapeString($conn,(trim($_POST['ewb_no'])));
$lrno = escapeString($conn,strtoupper(trim($_POST['lrno'])));
$branch = escapeString($conn,(trim($_POST['branch'])));
$narration = escapeString($conn,(trim($_POST['narration'])));

if($lrno=='' || $ewb_no=='' || $branch=='' || $narration=='')
{
	AlertErrorTopRight("Fill out all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ewb_no))
{
	AlertErrorTopRight("Invalid EWB number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(strlen($ewb_no)!=12)
{
	AlertErrorTopRight("Check EWB number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}


$chk_record = Qry($conn,"SELECT id FROM _allow_lr_ewb_expired WHERE lrno='$lrno' AND ewb_no='$ewb_no'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate record found for given LR number and EWB number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO _allow_lr_ewb_expired(lrno,ewb_no,branch,narration,username,approval_timestamp,timestamp) 
VALUES ('$lrno','$ewb_no','$branch','$narration','$user1','$timestamp','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	// echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>