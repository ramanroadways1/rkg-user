<?php
include("_header.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Ins_Exp') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<style>
label{
	font-size:12px !important;
}
</style>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Excel : Insurance Expiry Data </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
			
			<div class="col-md-12">
				
				<div class="row">
				
				<form action="download_ins_exp.php" method="POST" target="_blank">
					
					<div class="form-group col-md-3">
						<label>Get Records <font color="red"><sup>*</sup></font></label>
						<select onchange="GetByDate(this.value)" style="font-size:12px !important" name="record_type" id="record_type" class="form-control" required>
							<option style="font-size:12px !important" value="EXP_DATE">By Expiry Date</option>
							<option style="font-size:12px !important" value="ALL">ALL Records</option>
						</select>
					</div>
					
					<script>
					function GetByDate(elem)
					{
						$('#till_date').val('');
						
						if(elem=='ALL')
						{
							$('#exp_date_div').hide();
							$('#till_date').attr('required',false);
						}
						else
						{
							$('#exp_date_div').show();
							$('#till_date').attr('required',true);
						}
					}
					</script>
					
					<div class="form-group col-md-3" id="exp_date_div">
						<label>Till Expiry Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" id="till_date" name="till_date" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>	
								
					<div class="form-group col-md-3">
						<label>Get Empty Date Records <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="get_empty_date" class="form-control" required>
							<option style="font-size:12px !important" value="YES">YES</option>
							<option style="font-size:12px !important" value="NO">NO</option>
						</select>
					</div>
					
					<div class="form-group col-md-3">
						<label>&nbsp;</label>
						<?php
						if(!isMobile()){
							echo "<br>";
						}
						?>
						<button type="submit" name="btn_download" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
					</div>
				
				</form>				
				</div>
				
			</div>
				
				<div class="col-md-12">&nbsp;</div>
			
				
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>
	
<div id="func_result"></div> 
 