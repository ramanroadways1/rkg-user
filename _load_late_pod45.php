<?php
require_once("./_connect.php");

?>
<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>Total_LR_pending</th>
                        <th>LR_pending<br>(not shivani)</th>
                        <th>Last_Updated_By</th>
                        <th>Last_Updated_At</th>
                        <th>Remind_On</th>
						<th>#</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT id,branch,lr_45_days,lr_45_days_not_shivani,remind_on,allow_username,allow_timestamp 
	FROM _pending_lr WHERE branch NOT IN('DUMMY','HEAD') ORDER BY branch ASC");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='8'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			if($row['allow_timestamp']==''){
				$allow_timestamp = "NA";
			}
			else{					
				$allow_timestamp = date("d-m-y h:i A",strtotime($row['allow_timestamp']));
			}
			
			if($row['lr_45_days_not_shivani']>100){
				$btn_class = "btn-danger";
			}
			else{					
				$btn_class = "btn-warning";
			}
			
			$remind_on = date("d-m-y",strtotime($row['remind_on']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[branch]</td>
				<td><button alt='View LR' onclick=ViewLRs('with_shivani','$row[branch]') type='button' class='btn btn-warning btn-xs'><i class='fa fa-street-view' aria-hidden='true'></i> $row[lr_45_days]</button></td>
				<td><button alt='View LR' onclick=ViewLRs('not_shivani','$row[branch]') type='button' class='btn $btn_class btn-xs'><i class='fa fa-street-view' aria-hidden='true'></i> $row[lr_45_days_not_shivani]</button></td>
				<td>$row[allow_username]</td>
				<td>$allow_timestamp</td>
				<td><input id='remind_on_$row[id]' value='$row[remind_on]' type='date' class='form-control' min='".date('Y-m-d', strtotime('+1 day'))."' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}'></td>
				<td><button type='button' onclick='Update($row[id])' class='btn btn_allow btn-xs btn-success'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Update</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>
$('#loadicon').fadeOut('slow');
      $(function () {
        $("#example1").DataTable();
      });
</script>
				  