<?php
include("_header_datatable.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='EWB_Exemption') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Exempt Eway-Bill : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='EWB_Exemption') AND u_insert='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
				<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-3">
						<label>Exempt By</label>
						<select name="exempt_by" onchange="ExempyByFunc(this.value)" id="exempt_by" class="form-control" required="required">
							<option value="">--select an option--</option>
							<option value="LR_NO">LR Number</option>
							<option value="LR_SERIES">LR Series</option>
						</select>
					</div>
					
					<script>
						function ExempyByFunc(elem)
						{
							if(elem=='LR_SERIES')
							{
								$('.lrno_div').hide();
								$('.lr_series_div').show();
							}
							else
							{
								$('.lrno_div').show();
								$('.lr_series_div').hide();
							}
						}
						
						function Reset()
						{
							$("#lrno_to").val(''); 
							$("#lr_count").html(''); 
						}
						 
						 function Sum1()
						 {
							 $("#lr_count").val('');
							 
							 var from1 = $("#lrno_from").val();
							 var to1 = $("#lrno_to").val();
							 
							 if(from1=='')
							 {
								 $("#lr_count").val('');
								 $("#lrno_to").val('');
								 $("#lrno_from").focus();
							 }
							 else
							 {
								 $("#lr_count").html("Total : "+Number((Number(to1) - Number(from1))+1));
							 }
						}
						</script>
						
						<div class="lrno_div form-group col-md-3">
							<label>Enter LR number</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" type="text" class="form-control" id="lrno" />
						</div>
						
						<div style="display:none" class="lr_series_div form-group col-md-3">
							<label>From LR No. </label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');Reset();" type="text" class="form-control" id="lrno_from" />
						</div>
						
						<div style="display:none" class="lr_series_div form-group col-md-3">
							<label>To LR No. <sup><span style="color:red" id="lr_count"></span></sup></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');Sum1();" type="text" class="form-control" id="lrno_to" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
							<a href="_history_ewb_exempted.php" target="_blank"><button type="button" class="btn btn-sm pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				<?php
				}
				?>
				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var exempt_by = $('#exempt_by').val();
	var lrno = $('#lrno').val();
	var lrno_from = $('#lrno_from').val();
	var lrno_to = $('#lrno_to').val();
	
	if(exempt_by=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select exempted by option first !</font>',});
	}
	else
	{
		if(exempt_by=='LR_NO' && lrno=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter LR number first !</font>',});
		}
		else if(exempt_by=='LR_SERIES' && (lrno_from=='' || lrno_to==''))
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter LR series first !</font>',});
		}
		else
		{
			$('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_exempt_ewb.php",
				data: 'exempt_by=' + exempt_by + '&lrno=' + lrno + '&lrno_from=' + lrno_from + '&lrno_to=' + lrno_to,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
			});
		}
	}
}
</script>
  
<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_exempt_ewb.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
			 $('#example1').DataTable({ 
                 "destroy": true, //use for reinitialize datatable
              });
		},
		error: function() {}
	});
}

LoadTable();
</script>

<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='EWB_Exemption') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>	
function Approve(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "exempt_ewb_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
		error: function() {}
	});
	  }
	})
}

</script>
<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='EWB_Exemption') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>
function Reject(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "exempt_ewb_approve_reject.php",
			data: 'id=' + id + '&type=' + 'reject',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>