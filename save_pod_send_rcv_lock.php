<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,(($_POST['id'])));

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE user SET pod_lock='0' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Unlocked successfully !");
	echo "<script>
			$('#btn_allow_$id').attr('onclick','');
			$('#btn_allow_$id').html('Unlocked');
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>