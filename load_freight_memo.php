<?php
require_once("_connect.php");

$type = escapeString($conn,$_POST['type']);
$date = escapeString($conn,$_POST['date']);
$date_type = escapeString($conn,$_POST['date_type']);
$branch = escapeString($conn,$_POST['branch']);

if($branch=='ALL')
{
	$branch_col="";
}
else
{
	$branch_col="AND branch='$branch'";
}

if($date_type=='FM')
{
	$col_date="date";
}
else if($date_type=='ADV')
{
	$col_date="adv_date";
}
else
{
	$col_date="bal_date";
}
?>
<table id="example1" style="font-size:11px !important" class="table table-striped table-bordered">
	<thead>
    <tr>
        <th>#</th>
        <th>FM No</th>
        <th>Branch</th>
        <th>Total<br>Freight</th>
        <th>Adv</th>
        <th>Adv<br>Details</th>
        <th>Bal</th>
		<?php
		if($type=='YES')
		{
		?>
		<th>Total<br>Balance</th>
		<th>Date</th>
		<th>Bal<br>Details</th>
		<?php		
		}
		?>
    </tr>
    </thead>
									
    <tbody>
<?php
$qry = Qry($conn,"SELECT frno,branch,totalf,totaladv,cashadv,disadv,rtgsneftamt,baladv,totalbal,bal_date,paycash,paydsl,newrtgsamt 
FROM freight_form 
WHERE `$col_date`='$date' $branch_col ORDER by branch ASC,id ASC");
										
if(numRows($qry)>0)
{
	$i=1;
	
	while($row=fetchArray($qry))
	{
		echo "<tr>
			<td>$i</td>
			<td>
				<form target='_blank' action='../_view/freight_memo.php' method='POST'>
				<input type='hidden' value='$row[frno]' name='value1'>
				<input type='hidden' value='SEARCH' name='key'>
				<input type='submit' class='btn btn-primary btn-xs' value='$row[frno]' />
				</form>
			</td>
			<td>$row[branch]</td>
			<td>$row[totalf]</td>
			<td>$row[totaladv]</td>
			<td>
				<b>Cash</b> : $row[cashadv]<br>
				<b>Diesel</b> : $row[disadv]<br>
				<b>Rtgs</b> : $row[rtgsneftamt]
			</td>
			<td>$row[baladv]</td>";
		if($type=='YES')
		{
			echo "
			<td>$row[totalbal]</td>
			<td>$row[bal_date]</td>
			<td>
				<b>Cash</b> : $row[paycash]<br>
				<b>Diesel</b> : $row[paydsl]<br>
				<b>Rtgs</b> : $row[newrtgsamt]
			</td>";
		}													
		echo "</tr>";
		$i++;	
	}
}
else
{
	echo "<tr>
		<td colspan='8'>NO RESULT FOUND.</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>";
}
?>
 </tbody>
</table>

<script>										
	$('#loadicon').fadeOut('slow');
</script>								

<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>