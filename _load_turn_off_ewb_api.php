<?php
require_once("_connect.php");

?>
 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                         <th>Narration</th>
                         <th>Status</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT narration,is_active,timestamp FROM _by_pass_ewb");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='4'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
		
			if($row['is_active']=="1"){
				$status = "<font color='green'>Active</font>";
			}else{
				$status = "<font color='red'>In-Active</font>";
			}
			
			
			echo "<tr>
				<td>$i</td>
				<td>$row[narration]</td>
				<td>$status</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
				  
<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>