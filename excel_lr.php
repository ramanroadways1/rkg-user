<?php
include("_header.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Excel_LR') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<style>
label{
	font-size:12px !important;
}
</style>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Download : LR ENTRY : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
			
			<div class="col-md-12">
				
				<div class="row">
				
				<form action="download_lr_entry.php" method="POST" target="_blank">
					
					<div class="form-group col-md-2">
						<label>Date Type <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" name="record_by" class="form-control" required>
							<option style="font-size:12px !important" value="">--Date Type--</option>
							<option style="font-size:12px !important" value="LR">LR Date</option>
							<option style="font-size:12px !important" value="FM">Created On</option>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>From Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>	
								
					<div class="form-group col-md-2">
						<label>To Date <font color="red"><sup>*</sup></font></label>
						<input style="font-size:12px !important" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div class="form-group col-md-2">
						<label>Branch <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" id="branch" name="branch" class="form-control" required>
							<option style="font-size:12px !important" value="ALL">ALL Branches</option>
							<?php
							$qry = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
							
							if(numRows($qry)>0)
							{
								while($row = fetchArray($qry))
								{
									echo "<option style='font-size:12px !important' value='$row[username]'>$row[username]</option>";
								}
							}
							?>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>Only Shivani's Vehicle <font color="red"><sup>*</sup></font></label>
						<select style="font-size:12px !important" id="only_shivani" name="only_shivani" class="form-control" required>
							<option style="font-size:12px !important" value="NO">NO</option>
							<option style="font-size:12px !important" value="YES">YES</option>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>&nbsp;</label>
						<?php
						if(!isMobile()){
							echo "<br>";
						}
						?>
						<button type="submit" name="btn_download" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
					</div>
				
				</form>				
				</div>
				
			</div>
				
				<div class="col-md-12">&nbsp;</div>
			
				
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>
	
<div id="func_result"></div> 
 