<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y"); ?> RAMAN ROADWAYS PVT LTD.</strong> All rights reserved.
</footer>
	
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<script src="../admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>$.widget.bridge('uibutton', $.ui.button);</script>
<script src="../admin_lte/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../admin_lte/plugins/morris/morris.min.js"></script>
<script src="../admin_lte/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../admin_lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../admin_lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../admin_lte/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="../admin_lte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../admin_lte/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../admin_lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../admin_lte/plugins/fastclick/fastclick.min.js"></script>
<script src="../admin_lte/dist/js/app.min.js"></script>
<script src="../admin_lte/dist/js/pages/dashboard.js"></script>
<script src="../admin_lte/dist/js/demo.js"></script>
  </body>
</html>

<script type="text/javascript">
  $(window).load(function() {
    $("#loadicon").fadeOut("slow");;
  });
</script>