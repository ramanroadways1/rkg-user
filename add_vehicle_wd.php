<?php
include("_header_datatable.php");

$lr_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Add_Truck_WD') AND u_view='1'");
			  
if(numRows($lr_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Add market vehicle without documents : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Add_Truck_WD') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>					
				<div class="col-md-12">
				<div class="row">
						<div class="form-group col-md-3">
							<label>Vehicle No.</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="tno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Branch</label>
							<select class="form-control" id="branch">
								<option value="">--select branch--</option>
							<?php
							$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");
							
							if(numRows($get_branch)>0)
							{
								while($row_branch = fetchArray($get_branch))
								{
									echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
								}
							}
							?>							
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Vehicle Owner</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'');" type="text" class="form-control" id="owner_name" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Mobile Number</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');" maxlength="10" type="text" class="form-control" id="mobile_no" />
						</div>
						
						<div class="form-group col-md-3"> 
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
<?php
}
?>				
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Vehicle_Owner</th>
                        <th>Mobile_Number</th>
                        <th>Branch</th>
                        <th>Narration</th>
                        <th>Added_At</th>
                        <th>Added_By</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT tno,mo1,name,branch,branch_user,timestamp,narration FROM mk_truck WHERE doc_pending='1'");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='7'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[name]</td>
				<td>$row[mo1]</td>
				<td>$row[branch]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td>$row[branch_user]</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Add_Truck_WD') AND u_insert='1'");
			  
if(numRows($ewb_insert)>0)
{
?> 

<script>
function AddRecordFunc()
{
	var tno = $('#tno').val();
	var branch = $('#branch').val();
	var owner_name = $('#owner_name').val();
	var mobile_no = $('#mobile_no').val();
	var narration = $('#narration').val();
	
	if(tno=='' || branch=='' || owner_name=='' || mobile_no=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		// $('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_add_vehicle_wd.php",
				data: 'tno=' + tno + '&branch=' + branch + '&owner_name=' + owner_name + '&mobile_no=' + mobile_no + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
<?php
}
?> 