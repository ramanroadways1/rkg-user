<?php
include("_header_datatable.php");

$ewb_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Rtgs_Approval') AND u_view='1'");
			  
if(numRows($ewb_1)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Allow Rtgs Approval > 30 Days : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
					
					<div class="col-md-12 table-responsive" id="load_table_div">
                 
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_rtgs_approval.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table_div").html(data);
			 $('#example1').DataTable({ 
                 "destroy": true, //use for reinitialize datatable
              });
		},
		error: function() {}
	});
}

LoadTable();
</script>

<?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Rtgs_Approval') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>	
function Approve(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "rtgs_approval_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
		error: function() {}
	});
	  }
	})
}

</script>
<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Allow_Rtgs_Approval') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>
function Reject(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "rtgs_approval_approve_reject.php",
			data: 'id=' + id + '&type=' + 'reject',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>