<?php
require_once("_connect.php");

$branch = escapeString($conn,$_POST['branch']);
$date = escapeString($conn,$_POST['date']);
$exp_head = escapeString($conn,$_POST['exp_head']);

?>
<table id="example1" style="font-size:11px !important" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Vou_No</th>
                                            <th>Branch</th>
                                            <th>Date</th>
                                            <th>RR/RRPL</th>
                                            <th>Expense</th>
                                            <th>Amount</th>
                                            <th>Payment<br>By</th>
										</tr>
                                    </thead>
									
                                    <tbody>
                                        <?php
										if($branch=='ALL')
										{
											if($exp_head=='ALL')
											{
												$qry = Qry($conn,"SELECT user,vno,newdate,comp,des,amt,chq 
												FROM mk_venf WHERE newdate='$date' ORDER by user ASC,id ASC");
											}
											else
											{
												$qry = Qry($conn,"SELECT user,vno,newdate,comp,des,amt,chq 
												FROM mk_venf WHERE newdate='$date' AND desid='$exp_head' ORDER by user ASC,id ASC");
											}
										}
										else
										{
											if($exp_head=='ALL')
											{
												$qry = Qry($conn,"SELECT user,vno,newdate,comp,des,amt,chq 
												FROM mk_venf WHERE newdate='$date' AND user='$branch' ORDER by user ASC,id ASC");
											}
											else
											{
												$qry = Qry($conn,"SELECT user,vno,newdate,comp,des,amt,chq 
												FROM mk_venf WHERE newdate='$date' AND desid='$exp_head' AND user='$branch' ORDER by user ASC,id ASC");
											}
										}
										
										if(numRows($qry)>0)
										{
											$i=1;
											while($row=fetchArray($qry))
											{
												echo "<tr>
													<td>$i</td>
													<td>
														<form target='_blank' action='../_view/vouchers.php' method='POST'>
															<input type='hidden' value='$row[vno]' name='vou_no'>
															<input type='submit' class='btn btn-primary btn-xs' value='$row[vno]' />
														</form>
													</td>
													<td>$row[user]</td>
													<td>$row[newdate]</td>
													<td>$row[comp]</td>
													<td>$row[des]</td>
													<td>$row[amt]</td>
													<td>$row[chq]</td>
												</tr>";
											$i++;	
											}
										}
										else
										{
											echo "<tr>
													<td>NO RESULT FOUND.</td>
																								<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
												</tr>";
										}
										?>
                                    </tbody>
                                </table>

<script>										
	$('#loadicon').fadeOut('slow');
</script>								

<script>
      $(function () {
        $("#example1").DataTable();
      });
</script>								
