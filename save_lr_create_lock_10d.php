<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$branch = escapeString($conn,(($_POST['branch'])));
$remind_on = escapeString($conn,(($_POST['remind_on'])));
$narration = escapeString($conn,(($_POST['narration'])));

$chk_record = Qry($conn,"SELECT remind_on FROM lr_lock_extend WHERE branch='$branch' ORDER BY id DESC LIMIT 1");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($chk_record)>0)
{
	$row = fetchArray($chk_record);
	
	if($row['remind_on']>=date("Y-m-d"))
	{
		AlertErrorTopRight("Already exempted !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"INSERT INTO lr_lock_extend(branch,remind_on,narration,username,timestamp) VALUES ('$branch','$remind_on','$narration',
'$user1','$timestamp')");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Allowed successfully !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}	
?>