<?php
include("_header_datatable.php");

$fm_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unlock_FM_60D') AND u_view='1'");
			  
if(numRows($fm_3)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Allow Late Balance FM : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unlock_FM_60D') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>					
				<div class="col-md-12">
				<div class="row">
						
<script>			
function FetchVou(lrno)
{
	if(lrno!='')
	{
		$('#lrno').attr('readonly',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "_fetch_vou_no_by_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#vou_no").html(data);
		},
			error: function() {}
		});
	}
}
</script>						
						
						<div class="form-group col-md-3">
							<label>LR No.</label>
							<input autocomplete="off" onblur="FetchVou(this.value)" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Vou.No.</label>
							<select class="form-control" id="vou_no">
								<option value="">--select voucher--</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Narration</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="narration" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddRecordFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Record</button>
							<a href="_history_allow_late_balance.php" target="_blank"><button type="button" class="btn btn-sm pull-right btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-street-view" aria-hidden="true"></i> &nbsp; History</button></a>
						</div>
						
				</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				<?php
				}
				?>
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>POD_date</th>
                        <th>Days_Diff.</th>
                        <th>Vou_No</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>#Approve</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT e.id,e.lrno,e.frno as vou_no,e.lr_date,e.narration,e.branch,e.timestamp,u.name,
(SELECT pod_date FROM rcv_pod WHERE frno=e.frno ORDER BY pod_date DESC LIMIT 1) as pod_date	
	FROM extend_bal_pending_validity AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user 
	WHERE e.admin_approval_timestamp IS NULL");
	
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$lr_date = date("d-m-y",strtotime($row['lr_date']));
			$pod_date = date("d-m-y",strtotime($row['pod_date']));
			
			$diff_days = round((strtotime(date("Y-m-d")) - strtotime($row['lr_date'])) / (60 * 60 * 24))+1;
			
			if($diff_days>90){
				$diff_days = "<font color='red'><b>$diff_days</b></font>";	
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[lrno]</td>
				<td>$lr_date</td>
				<td>$pod_date</td>
				<td>$diff_days</td>
				<td>$row[vou_no]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
				<td><button type='button' id='btn_allow_$row[id]' onclick='Approve($row[id])' class='btn btn_approve btn-success btn-xs'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Approve</button></td>
				<td><button type='button' id='btn_reject_$row[id]' onclick='Reject($row[id])' class='btn btn_reject btn-danger btn-xs'><i class='fa fa-ban' aria-hidden='true'></i> Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  

<script>
function AddRecordFunc()
{
	var lrno = $('#lrno').val();
	var vou_no = $('#vou_no').val();
	var narration = $('#narration').val();
	
	if(lrno=='' || vou_no=='' || narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill out all fields first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "save_allow_late_balance.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&narration=' + narration,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
  
 <?php
$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unlock_FM_60D') AND u_update='1'");
			  
if(numRows($ewb_insert)>0)
{
?>  
<script>	
function Approve(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "allow_late_balance_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}

</script>


<?php
}
else
{
	echo "<script>$('.btn_approve').attr('disabled',true);</script>";
}

$ewb_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[user_rkg]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='1005' AND func_name='Unlock_FM_60D') AND u_delete='1'");
			  
if(numRows($ewb_insert)>0)
{
?>
<script>

function Reject(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#loadicon').show();
		jQuery.ajax({
			url: "allow_late_balance_approve_reject.php",
			data: 'id=' + id + '&type=' + 'approve',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	  }
	})
}
</script>

<?php
}
else
{
	echo "<script>$('.btn_reject').attr('disabled',true);</script>";
}
?>
